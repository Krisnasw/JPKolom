<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => ['guest', 'XSS']], function () {

	Route::get('/', 'AuthController@index');
	Route::post('/mvi-admin/api/login', 'AuthController@doLogin');
	Route::get('/register', 'AuthController@getRegister');
	Route::post('/mvi-admin/api/register', 'AuthController@doRegister');
	Route::get('/forget-password', 'AuthController@getRecovery');
	Route::post('/mvi-admin/api/recovery', 'AuthController@doRecovery');
	Route::get('/mvi-admin', function () { return redirect('/'); });

	// Verify User
	Route::get('/verify', 'AuthController@verify')->name('signup.verify');
	Route::get('/recovery', 'AuthController@Recovery')->name('mvi.lupas');

});

Route::group(['middleware' => ['auth', 'XSS']], function () {

	// All Roles
	Route::get('/mvi-admin/home', 'AuthController@getHome');
	Route::get('/mvi-admin/auth', 'AuthController@getAuthUser');
	Route::post('/mvi-admin/auth', 'AuthController@updateUserLogin');
	Route::get('/mvi-admin/pasang-iklan', 'AuthController@getAdvertisement');
	Route::get('/mvi-admin/api/ajax-jenis', 'AuthController@getAjaxJenis');
	Route::get('/mvi-admin/api/ajax-kategori', 'AuthController@getAjaxKategori');
	Route::get('/mvi-admin/api/ajax-paket', 'AuthController@getAjaxPaket');
	Route::post('/mvi-admin/api/pasang-iklan', 'AuthController@postAdv');
	Route::get('/mvi-admin/faq', 'AuthController@getFaq');
	Route::post('/mvi-admin/api/post-faq', 'AuthController@postFaq');
	Route::post('/mvi-admin/api/post-komen', 'AuthController@postKomen');
	Route::get('/mvi-admin/api/a-paket', 'AuthController@getPaket');
	Route::get('/mvi-admin/api/a-template', 'AuthController@getTemplateJSON');

	// Customer
	Route::get('/mvi-admin/m/list-order', 'AuthController@getMemberOrder');
	Route::get('/mvi-admin/m/history-order', 'AuthController@getHistoriOrderByMember');
	Route::get('/mvi-admin/m/show-order/{id}/show', 'AuthController@getDetailOrder');
	Route::get('/mvi-admin/m/cancel-order/{id}', 'AuthController@canceledOrder');
	Route::get('/mvi-admin/m/pembayaran', 'AuthController@getPembayaran');
	Route::get('/mvi-admin/m/history-pembayaran', 'AuthController@getListPembayaran');
	Route::post('/mvi-admin/api/bayar', 'AuthController@postBukti');
	Route::get('/mvi-admin/export/pdf/{id}', 'AuthController@getPDF');
	Route::get('/mvi-admin/m/new-design', 'AuthController@getNewDesign');

	// Resource
	Route::resource('/mvi-admin/customer', 'UserController');
	Route::resource('/mvi-admin/kategori', 'KategoriController');
	Route::resource('/mvi-admin/media', 'MediaController');
	Route::resource('/mvi-admin/jenis-iklan', 'IklanController');
	Route::resource('/mvi-admin/ukuran', 'UkuranController');
	Route::resource('/mvi-admin/paket', 'PaketController');
	Route::resource('/mvi-admin/metode-bayar', 'MetodeController');
	Route::resource('/mvi-admin/order', 'OrderController');
	Route::resource('/mvi-admin/pembayaran', 'PaymentController');
	Route::resource('/mvi-admin/template', 'DesignController');

	// Mimin
	Route::get('/mvi-admin/pending-order', 'OrderController@getPendingOrder');

	Route::get('/mvi-admin/logout', 'AuthController@doLogout');

});