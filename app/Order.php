<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'order_iklan';
    protected $primaryKey = 'order_id';
    protected $fillable = ['media_id', 'iklan_id', 'kategori_id', 'paket_id', 'customer_id', 'metode_id', 'tgl_muat', 'tgl_akhir', 'order_no', 'total_biaya', 'judul_iklan', 'status_iklan', 'description', 'order_design', 'order_due', 'harga_asli', 'ppn', 'deleted'];

    public function categories()
    {
    	return $this->belongsTo('App\Kategori', 'kategori_id');
    }

    public function medias()
    {
    	return $this->belongsTo('App\Media', 'media_id');
    }

    public function jenis()
    {
    	return $this->belongsTo('App\JenisIklan', 'iklan_id');
    }

    public function pakets()
    {
    	return $this->belongsTo('App\Paket', 'paket_id');
    }

    public function metodes()
    {
        return $this->belongsTo('App\Metode', 'metode_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }
}
