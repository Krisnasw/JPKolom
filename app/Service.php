<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    protected $table = 'faq';
    protected $primaryKey = 'faq_id';
    protected $fillable = ['customer_id', 'faq_title', 'faq_slug', 'faq_description'];

    public function users()
    {
    	return $this->belongsTo('App\User', 'customer_id', 'id');
    }
}
