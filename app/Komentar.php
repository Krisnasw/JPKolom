<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    //
    protected $table = 'komentar';
    protected $primaryKey = 'komen_id';
    protected $fillable = ['customer_id', 'faq_id', 'komen_isi'];

    public function users()
    {
    	return $this->belongsTo('App\User', 'customer_id' ,'id');
    }

    public function services()
    {
    	return $this->belongsTo('App\Service', 'faq_id');
    }
}
