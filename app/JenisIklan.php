<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisIklan extends Model
{
    //
    protected $table = 'jenis_iklan';
    protected $primaryKey = 'iklan_id';
    protected $fillable = ['media_id', 'iklan_type', 'iklan_slug'];

    public function medias()
    {
    	return $this->belongsTo('App\Media', 'media_id');
    }
}