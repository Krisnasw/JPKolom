<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ukuran extends Model
{
    //
    protected $table = 'ukuran';
    protected $primaryKey = 'ukuran_id';
    protected $fillable = ['ukuran_kolom', 'ukuran_mm'];
    public $timestamps = false;
}
