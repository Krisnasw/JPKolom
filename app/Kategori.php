<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //
    protected $table = 'kategori';
    protected $primaryKey = 'kategori_id';
    protected $fillable = ['iklan_id', 'kategori_name', 'kategori_slug'];

    public function adv()
    {
    	return $this->belongsTo('App\JenisIklan', 'iklan_id');
    }
}
