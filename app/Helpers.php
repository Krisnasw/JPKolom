<?php

namespace App;

use DB;

class Helpers {

	public static function convertdate() {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('dmy');
        return $date;
    }

    public static function autonumber($order,$primary,$prefix){
        $q = DB::table($order)->select(DB::raw('MAX(RIGHT('.$primary.',5)) as kd_max'));
        $prx = $prefix;
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kd_max)+1;
                $kd = $prx.sprintf("%06s", $tmp);
            }
        }
        else
        {
            $kd = $prx."000001";
        }

        return $kd;
    }

	public static function tgl_indo($tgl)
	{
     	$tanggal = substr($tgl,8,2);
     	switch (substr($tgl,5,2)){
					case '01': 
						$bulan= "Januari";
						break;
					case '02':
						$bulan= "Februari";
						break;
					case '03':
						$bulan= "Maret";
						break;
					case '04':
						$bulan= "April";
						break;
					case '05':
						$bulan= "Mei";
						break;
					case '06':
						$bulan= "Juni";
						break;
					case '07':
						$bulan= "Juli";
						break;
					case '08':
						$bulan= "Agustus";
						break;
					case '09':
						$bulan= "September";
						break;
					case '10':
						$bulan= "Oktober";
						break;
					case '11':
						$bulan= "November";
						break;
					case '12':
						$bulan= "Desember";
						break;
				}

		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;
     }

    public static function read_more($string,$limit=100)
	{
		$length = strlen(strip_tags($string));
		if ($length>$limit) {
			$isi_halaman = strip_tags($string);
			$isi = substr($isi_halaman,0,$limit);
			$isi = substr($isi_halaman,0,strrpos($isi," "));

			return $isi;

		}
		else {
			return strip_tags($string);
		}
	}

	public static function find_replace($string,$find)
	{
		$bodytag = str_replace($find, "<b><u>".$find."</u></b>", $string);
		return $bodytag;
	}

	public static function goResult($def,$msg)
	{
    	$response['status'] = $def;
		$response['msg'] 	= $msg;

	    header('Content-Type: application/json');
		return json_encode($response);
    }

	public static function img_holder($type=null){
		switch ($type) {
			case 'food':
				return '/images/placeholder/food.png';
				break;
			case 'profile':
			    return '/images/placeholder/avatar.png';
				break;
			default:
				return url('images/placeholder/placeholder.png');
				# code...
				break;
		}
	}

}

?>