<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metode extends Model
{
    //
    protected $table = 'metode';
    protected $primaryKey = 'metode_id';
    protected $fillable = ['metode_bank', 'metode_an', 'metode_norek'];
    public $timestamps = false;
}
