<?php

namespace App\Http\Middleware;

use Closure;

class isRoot
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check() && \Auth::user()->roles_id != 3) {
            # code...
            return redirect('/mvi-admin/home');
        }

        return $next($request);
    }
}
