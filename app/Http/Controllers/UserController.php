<?php

namespace App\Http\Controllers;

use App\User;
use Alert;
use Validator;
use File;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = User::where('roles_id', '!=', 3)->get();
        return view('user.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type = 'edit';
        $data = User::where('id', base64_decode($id))->first();
        return view('user.content', ['data' => $data, 'type' => $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
                'name' => 'required',
                'address' => 'required',
                'contact' => 'required',
                'username' => 'required',
                'email' => 'required',
                'image' => 'image|mimes:jpg,jpeg,png'
            ]);

        if ($valid->passes()) {
            # code...
            $data = User::findOrFail(base64_decode($id));
            $data->name = $request->name;
            $data->address = $request->address;
            $data->contact = $request->contact;
            $data->username = $request->username;
            $data->email = $request->email;
            $data->password = bcrypt($request->password);
            $data->image = ($request->hasFile('image')) ? $this->savePhoto($request->file('image')) : $data->image;

            if ($data->save()) {
                # code...
                echo Helpers::goResult(true, 'User Berhasil Diupdate!');
                return;
            }

            echo Helpers::goResult(false, 'Foto Profil Wajib Diisi!');
            return;
        } else {
            Alert::info('Data Tidak Valid!', 'Info!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = User::findOrFail(base64_decode($id));
        $deletePhoto = $this->deletePhoto($del->image);

        if ($del->delete()) {
            # code...
            Alert::success('Customer Telah Dihapus', 'Success!');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Customer', 'Error!');
            return redirect()->back();
        }
    }

    // Protected Function
    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'user';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $up['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $up['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
