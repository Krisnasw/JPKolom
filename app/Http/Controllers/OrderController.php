<?php

namespace App\Http\Controllers;

use App\Order;
use App\Helpers;
use Alert;
use Validator;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Order::where('status_iklan', '!=', 'canceled')->orderBy('order_id', 'desc')->get();
        return view('order.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Order::where('order_id', base64_decode($id))->get();
        return view('order.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Order::where('order_id', base64_decode($id))->first();
        return view('order.content', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $q = Order::updateOrCreate(['order_id' => base64_decode($id)], [
                    'status_iklan' => 'paid'
                ]);

        if ($q) {
            # code...
            Alert::success('Order Telah Dikonfirmasi', 'Success!');
            return redirect()->back();
        }

        Alert::error('Gagal Konfirmasi Order', 'Error!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Order::findOrFail(base64_decode($id));
        $del->status_iklan = 'canceled';
        $del->save();

        if ($del) {
            # code...
            Alert::success('Order Telah Dibatalkan', 'Success!');
            return redirect()->back();
        }

        Alert::error('Gagal Membatalkan Order', 'Error!');
        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPendingOrder()
    {
        $data = Order::where('status_iklan', 'canceled')->orderBy('order_id', 'desc')->get();
        return view('order.pending', ['data' => $data]);
    }
}
