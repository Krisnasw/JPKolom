<?php

namespace App\Http\Controllers;

use App\Paket;
use App\Ukuran;
use App\Template;
use App\Helpers;
use Alert;
use Validator;
use File;
use Illuminate\Http\Request;

class DesignController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
        $uk = Ukuran::orderBy('ukuran_id', 'desc')->get();
        view()->share('uk', $uk);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Template::orderBy('template_id', 'desc')->get();
        return view('design.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('design.content', ['type' => 'create', 'uk' => $uk]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
                'paket_id' => 'required',
                'ukuran_id' => 'required',
                'image' => 'required|image|mimes:jpg,jpeg,png',
                'name' => 'required|string'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Template::firstOrCreate([
                    'paket_id' => $request->paket_id,
                    'ukuran_id' => $request->ukuran_id,
                    'template_design' => $this->savePhoto($request->file('image')),
                    'template_name' => $request->name
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Template Berhasil Ditambahkan');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Menambahkan Template');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Template::where('template_id', base64_decode($id))->first();
        return view('design.content', ['data' => $data, 'type' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
                'paket_id' => 'required',
                'ukuran_id' => 'required',
                'image' => 'image|mimes:jpg,jpeg,png',
                'name' => 'required|string'
            ]);

        if ($valid->passes()) {
            # code...
            $cek = Template::findOrFail(base64_decode($id));

            $q = Template::updateOrCreate(['template_id' => base64_decode($id)], [
                    'paket_id' => $request->paket_id,
                    'ukuran_id' => $request->ukuran_id,
                    'template_design' => ($request->hasFile('image')) ? $this->savePhoto($request->file('image')) : $cek->template_design,
                    'template_name' => $request->name
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Template Berhasil Diupdate');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Memperbarui Template');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Template::findOrFail(base64_decode($id));
        $deletePhoto = $this->deletePhoto($del->template_design);

        if ($del->delete()) {
            # code...
            Alert::success('Template Telah Dihapus', 'Success!');
            return redirect()->back();
        }

        Alert::error('Gagal Hapus Template', 'Error!');
        return redirect()->back();
    }

    // Protected Function
    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'template';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $q['template_design'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $q['template_design'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}