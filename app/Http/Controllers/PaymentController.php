<?php

namespace App\Http\Controllers;

use App\Konfirmasi;
use App\Helpers;
use Carbon\Carbon;
use Auth;
use Alert;
use Validator;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Konfirmasi::orderBy('konfirmasi_id', 'desc')->get();
        return view('pembayaran.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = Konfirmasi::updateOrCreate(['konfirmasi_id' => base64_decode($id)], [
                'status_payment' => 'y',
                'k_tgl_approve' => Carbon::now(),
                'k_approve_by' => Auth::user()->name
            ]);

        if ($data) {
            # code...
            Alert::success('Konfirmasi Pembayaran Berhasil', 'Success!');
            return redirect()->back();
        }

        Alert::error('Gagal Konfirmasi Pembayaran', 'Error!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
