<?php

namespace App\Http\Controllers;

use App\Media;
use App\Helpers;
use Alert;
use Validator;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Media::orderBy('media_id', 'desc')->get();
        return view('media.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('media.content', ['type' => 'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
                'media_name' => 'required|string|max:50'
            ]);

        if ($valid->passes()) {
            # code...
            $exec = Media::firstOrCreate([
                    'media_name' => $request->media_name,
                    'media_slug' => str_slug($request->media_name)
                ]);

            if ($exec) {
                # code...
                echo Helpers::goResult(true, 'Media Berhasil Ditambahkan');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Tambah Media');
            return;
        }

        echo Helpers::goResult(false, 'Data Tidak Valid');
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Media::where('media_id', base64_decode($id))->first();
        return view('media.content', ['type' => 'edit', 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
                'media_name' => 'required|string|max:50'
            ]);

        if ($valid->passes()) {
            # code...
            $exec = Media::updateOrCreate(['media_id' => base64_decode($id)], [
                    'media_name' => $request->media_name,
                    'media_slug' => str_slug($request->media_name)
                ]);

            if ($exec) {
                # code...
                echo Helpers::goResult(true, 'Media Berhasil Ditambahkan');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Tambah Media');
            return;
        }

        echo Helpers::goResult(false, 'Data Tidak Valid');
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Media::findOrFail(base64_decode($id));

        if ($del->delete()) {
            # code...
            Alert::success('Media Telah Dihapus', 'Success!');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Media', 'Error!');
            return redirect()->back();
        }
    }
}
