<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Kategori;
use App\Media;
use App\JenisIklan;
use App\Ukuran;
use App\Komentar;
use App\Service;
use App\Paket;
use App\Helpers;
use App\Metode;
use App\Template;
use App\Order;
use App\Konfirmasi;
use App\Mail\VerifyEmail;
use Alert;
use DB;
use Mail;
use Crypt;
use Password;
use File;
use Validator;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller
{
    //
    public function index()
    {
    	if (!Auth::check()) {
    		# code...
    		return view('welcome');
    	} else {
    		# code...
    		return redirect('/mvi-admin/home');
    	}
    }

    public function doLogin(Request $request)
    {
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($valid->passes()) {
            # code...
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'status' => 'Y'])) {
                # code...
                echo Helpers::goResult(true, "Selamat Datang!");
                return;
            } else {
                echo Helpers::goResult(false, "Username Atau Password Salah!");
                return;
            }
        }

        echo Helpers::goResult(false, "Data Tidak Valid, Coba Lagi!");
        return;
    }

    public function getRegister()
    {
    	if (!Auth::check()) {
    		# code...
    		return view('register');
    	}

    	return redirect('/mvi-admin/home');
    }

    public function doRegister(Request $request)
    {
    	if (!$request->ajax()) {
    		# code...
    		exit('no direct scripts allowed');
    	}

    	$valid = Validator::make($request->all(), [
    			'name' => 'required|string|max:50',
    			'username' => 'required|string|max:50',
    			'email' => 'required|max:50',
    			'r_email' => 'required|max:50|same:email',
    			'password' => 'required',
    			'r_password' => 'required|same:password',
    			'role' => 'required',
    			'contact' => 'required',
    			'address' => 'required',
    			'address_2' => 'required',
                'captcha' => 'required|captcha'
    		]);

    	if ($valid->passes()) {
    		# code...
    		// dd($valid);
    		$q = DB::transaction(function () use ($request) {

    			$user = User::create([
    					'name' => $request->name,
    					'username' => $request->username,
			            'email' => $request->r_email,
			            'password' => bcrypt($request->r_password),
			            'roles_id' => $request->role,
			            'contact' => $request->contact,
			            'address' => $request->address,
			            'address_2' => $request->address_2,
			            'npwp' => $request->npwp,
			            'c_person' => $request->cp
    				]);

    			Mail::to($user->email)->send(new VerifyEmail($user));
    		});

    		if ($q) {
		    	# code...
		    	echo Helpers::goResult(false, 'Pendaftaran Gagal, Silahkan Coba Lagi!');
		    	return;
		    } else {
		    	# code...
		    	echo Helpers::goResult(true, 'Pendaftaran Sukses, Silahkan Cek Email Anda!');
		    	return;
		    }
    	}

    	echo Helpers::goResult(false, "Register Gagal, Data Tidak Valid!");
    	return;
    }

    public function verify()
    {
    	if (empty(request('token'))) {
	        return redirect('/register');
	    }
	    
	    $decryptedEmail = Crypt::decrypt(request('token'));

	    $user = User::whereEmail($decryptedEmail)->first();
	    if ($user->status == 'y') {

	        return redirect('/');
	    }

	    $user->status = 'y';
	    $user->save();

	    Auth::loginUsingId($user->id);
	    
	    return redirect('/');
    }

    public function getHome()
    {
        return view('dashboard', [
                'cst' => User::where('roles_id', '!=', '3')->count(),
                'ord' => Order::count(),
                'pnd' => Order::where('status_iklan', 'canceled')->count()
            ]);
    }

    public function doLogout()
    {
        Auth::logout();
        Alert::success('Terima Kasih Telah Login!', 'Success!');

        return redirect('/');
    }

    public function getRecovery()
    {
        return view('forget');
    }

    public function doRecovery(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'email' => 'required'
            ]);

        if ($valid->passes()) {
            # code...
            $cek = User::whereEmail($request->email)->first();

            if ($cek) {
                # code...
                // Mail::to($request->email)->send(new PasswordRcv($cek));
                $go = Password::sendResetLink(['email' => $request->email]);

                if ($go) {
                    # code...
                    echo Helpers::goResult(true, 'Silahkan Cek Email Anda!');
                    return;
                }

                echo Helpers::goResult(false, 'Gagal Lupa Password!');
                return;
            }

            echo Helpers::goResult(false, 'Email Tidak Ditemukan!');
            return;
        }

        echo Helpers::goResult(false, 'Email Masih Kosong!');
        return;
    }

    public function getAuthUser()
    {
        return view('setting.index');
    }

    public function updateUserLogin(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'fullname' => 'required',
            'username' => 'required',
            'email' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'image' => 'mimes:jpg,jpeg,png,gif|image'
        ]);

        if ($valid->passes()) {
            # code...
            $up = User::findOrFail(Auth::user()->id);

            if (!empty($request->passwordold) && !empty($request->password) && !empty($request->passwordconf)) {
              # code...
              if ($request->hasFile('image')) {
                # code...
                $up->name = $request->fullname;
                $up->email = $request->email;
                $up->username = $request->username;
                $up->address = $request->address;
                $up->contact = $request->contact;
                $up->image = $this->deletePhoto($up->image);
                $up->image = ($request->hasFile('image')) ? $this->savePhoto($request->file('image')) : 'images/user/person_placeholder.png';
                $up->password = $request->passwordconf;

                if ($up->save()) {
                    # code...
                    echo Helpers::goResult(true, "Data Telah Di Update");
                    return;
                }

                echo Helpers::goResult(false, "Data Gagal Diupdate");
                return;

              } else {

                $up->name = $request->fullname;
                $up->email = $request->email;
                $up->username = $request->username;
                $up->address = $request->address;
                $up->contact = $request->contact;
                $up->image = $up->image;
                $up->password = $request->passwordconf;

                if ($up->save()) {
                    # code...
                    echo Helpers::goResult(true, "Data Telah Di Update");
                    return;
                }

                    echo Helpers::goResult(false, "Data Gagal Diupdate");
                    return;
                }
            } else {
                $up->name = $request->fullname;
                $up->email = $request->email;
                $up->username = $request->username;
                $up->address = $request->address;
                $up->contact = $request->contact;
                $up->image = $this->deletePhoto($up->image);
                $up->image = ($request->hasFile('image')) ? $this->savePhoto($request->file('image')) : 'images/user/person_placeholder.png';

                if ($up->save()) {
                    # code...
                    echo Helpers::goResult(true, "Data Telah Di Update");
                    return;
                }

                echo Helpers::goResult(false, "Data Gagal Diupdate");
                return;
            }
        } else {

            Alert::error('Gagal Update Profile', 'Error!');
            return redirect()->back();
        }
    }

    public function getAdvertisement()
    {
        $table = "order_iklan";
        $primary = "order_id";
        $prefix = "JPK-";
        $data = Helpers::autonumber($table, $primary, $prefix);

        return view('iklan.index', ['kat' => Kategori::all(),
                                    'media' => Media::all(),
                                    'jenis' => JenisIklan::all(),
                                    'paket' => Paket::all(),
                                    'metode' => Metode::all(),
                                    'nomer' => $data
                                   ]);
    }

    public function getAjaxJenis()
    {
        $state_id = Input::get('media_id');
        $data = JenisIklan::where('media_id', $state_id)->get();
        return response()->json($data);
    }

    public function getAjaxKategori()
    {
        $state_id = Input::get('iklan_id');
        $data = Kategori::where('iklan_id', $state_id)->get();
        return response()->json($data);
    }

    public function getAjaxPaket()
    {
        $state_id = Input::get('kategori_id');
        $data = Paket::where('kategori_id', $state_id)->get();
        return response()->json($data);
    }

    public function postAdv(Request $request)
    {
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
                'media_id' => 'required',
                'iklan_id' => 'required',
                'kategori_id' => 'required',
                'paket_id' => 'required',
                'metode_id' => 'required',
                'tgl_awal' => 'required',
                'tgl_akhir' => 'required',
                'regone' => 'required',
                'ppn' => 'required',
                'total_biaya' => 'required',
                'no_order' => 'required',
                'judul_iklan' => 'required|string',
                'paket_description' => 'required|string',
                'image' => 'image|mimes:jpg,jpeg,png'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Order::firstOrCreate([
                    'media_id' => $request->media_id,
                    'iklan_id' => $request->iklan_id,
                    'kategori_id' => $request->kategori_id,
                    'paket_id' => $request->paket_id,
                    'customer_id' => Auth::user()->id,
                    'metode_id' => $request->metode_id,
                    'tgl_muat' => $request->tgl_awal,
                    'tgl_akhir' => $request->tgl_akhir,
                    'order_no' => $request->no_order,
                    'harga_asli' => $request->regone,
                    'ppn' => $request->ppn,
                    'total_biaya' => $request->total_biaya,
                    'judul_iklan' => $request->judul_iklan,
                    'description' => $request->paket_description,
                    'order_design' => ($request->file('image')) ? $this->saveUploader($request->file('image')) : 'images/placeholder/placeholder.png',
                    'order_due' => Date('y:m:d', strtotime("+5 days"))
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Pesanan Berhasil Diterima, Silahkan Melunasi Pembayaran');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Order Iklan');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid');
        return;
    }

    public function getMemberOrder()
    {
        $data = Order::where([
                ['customer_id', Auth::user()->id],
                ['deleted', 'n']
            ])->orderBy('order_id', 'desc')->get();
        return view('order.member.index', ['data' => $data]);
    }

    public function getHistoriOrderByMember()
    {
        $data = Order::where('customer_id', Auth::user()->id)->orderBy('order_id', 'desc')->get();
        return view('order.member.histori', ['data' => $data]);
    }

    public function getDetailOrder($id)
    {
        $data = Order::where('order_id', base64_decode($id))->get();
        return view('order.member.show', ['data' => $data]);
    }

    public function canceledOrder($id)
    {
        $data = Order::updateOrCreate(['order_id' => base64_decode($id)], [
                'deleted' => 'Y'
            ]);

        if ($data) {
            # code...
            Alert::success('Order Berhasil Dibatalkan', 'Success!');
            return redirect()->back();
        }

        Alert::error('Gagal Membatalkan Order', 'Error!');
        return redirect()->back();
    }

    public function getPDF($id)
    {
        $data = Order::where('order_id', base64_decode($id))->get();
        $pdf = PDF::loadView('pdf', ['data' => $data]);
        return $pdf->download();
    }

    public function getPembayaran()
    {
        return view('order.member.payment', ['metod' => Metode::all()]);
    }

    public function getListPembayaran()
    {
        $data = Konfirmasi::where('customer_id', Auth::user()->id)->orderBy('konfirmasi_id', 'desc')->get();
        return view('order.member.histori-payment', ['data' => $data]);
    }

    public function postBukti(Request $request)
    {
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
                'no_order' => 'required',
                'name_p' => 'required|string',
                'bank_p' => 'required|string',
                'b_penerima' => 'required',
                'nominal' => 'required|numeric',
                'tgl_tf' => 'required',
                'image' => 'required|image|mimes:jpg,jpeg,png'
            ]);

        if ($valid->passes()) {
            # code...
            $cari = Order::where('order_no', $request->no_order)->get();

            if ($cari) {
                # code...
                $q = Konfirmasi::firstOrCreate([
                    'customer_id' => Auth::user()->id,
                    'order_no' => $request->no_order,
                    'k_pengirim' => $request->name_p,
                    'k_bank_pengirim' => $request->bank_p,
                    'metode_id' => $request->b_penerima,
                    'k_nominal' => $request->nominal,
                    'k_tgl_transfer' => $request->tgl_tf,
                    'bukti' => $this->saveBukti($request->file('image'))
                ]);

                if ($q) {
                    # code...
                    echo Helpers::goResult(true, 'Upload Bukti Berhasil, Harap Bersabar');
                    return;
                }

                echo Helpers::goResult(false, 'Gagal Konfirmasi Pembayaran');
                return;
            }
            
            echo Helpers::goResult(false, 'No Order Tidak Ditemukan');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    public function getNewDesign()
    {
        $uk = Ukuran::orderBy('ukuran_id', 'desc')->get();
        return view('design.member.index', ['uk' => $uk]);
    }

    public function getTemplateJSON()
    {
        $state_id = Input::get('paket_id');
        $data = Template::where('paket_id', $state_id)->get();
        return response()->json($data);
    }

    public function getFaq()
    {
        $data = Service::orderBy('faq_id', 'desc')->get();
        $cmnt = Komentar::orderBy('komen_id', 'desc')->get();
        return view('supp.index', ['data' => $data, 'komen' => $cmnt]);
    }

    public function postFaq(Request $request)
    {
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
                'judul' => 'required|string',
                'isi' => 'required|string'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Service::firstOrCreate([
                    'customer_id' => Auth::user()->id,
                    'faq_title' => $request->judul,
                    'faq_slug' => str_slug($request->judul),
                    'faq_description' => $request->isi
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Pertanyaan Berhasil Dibuat');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Membuat Pertanyaan');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    public function postKomen(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'komen' => 'required|string'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Komentar::firstOrCreate([
                    'customer_id' => Auth::user()->id,
                    'faq_id' => $request->id_faq,
                    'komen_isi' => $request->komen
                ]);

            if ($q) {
                # code...
                Alert::success('Komentar Telah Ditambahkan', 'Success!');
                return redirect()->back();
            }

            Alert::error('Gagal Menambahkan Komentar', 'Error!');
            return redirect()->back();

        }

        Alert::info('Data Tidak Valid!', 'Info!');
        return redirect()->back();
    }

    public function getPaket()
    {
        $state_id = Input::get('ukuran_id');
        $data = Paket::where('ukuran_id', $state_id)->get();
        return response()->json($data);
    }

    // Protected Function
    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'user';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $up['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $up['image'];
    }

    protected function saveUploader($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'order';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $q['order_design'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $q['order_design'];
    }

    protected function saveBukti($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'bukti';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $q['bukti'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $q['bukti'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }

}