<?php

namespace App\Http\Controllers;

use SEOMeta;
use OpenGraph;
use Twitter;
use SEO;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
    	SEOMeta::setTitle('Advertisement Information System');
        SEOMeta::setDescription('Advertisement Information System, Iklan Kolom Jawa Pos, Surabaya, Indonesia');

        OpenGraph::setDescription('Advertisement Information System, Iklan Kolom Jawa Pos, Surabaya, Indonesia');
        OpenGraph::setTitle('Advertisement Information System');
        OpenGraph::setUrl('http://localhost/ikolom');
        OpenGraph::addProperty('type', 'web-based-system');

        Twitter::setTitle('Advertisement Information System');
        Twitter::setSite('@JawaPos');
    }
}
