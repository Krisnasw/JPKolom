<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Helpers;
use App\JenisIklan;
use Alert;
use Validator;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
        $jk = JenisIklan::orderBy('iklan_id', 'desc')->get();
        view()->share('iklan', $jk);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Kategori::orderBy('kategori_id', 'desc')->get();
        return view('kategori.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('kategori.content', ['type' => 'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
                'jenis' => 'required',
                'kategori_name' => 'required|string|max:50'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Kategori::firstOrCreate([
                    'iklan_id' => $request->jenis,
                    'kategori_name' => $request->kategori_name,
                    'kategori_slug' => str_slug($request->kategori_name)
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Kategori Telah Ditambahkan');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Tambah Kategori');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type = 'edit';
        $data = Kategori::where('kategori_id', base64_decode($id))->first();
        return view('kategori.content', ['data' => $data, 'type' => $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
                'jenis' => 'required',
                'kategori_name' => 'required|string|max:50'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Kategori::updateOrCreate(['kategori_id' => base64_decode($id)], [
                    'iklan_id' => $request->jenis,
                    'kategori_name' => $request->kategori_name,
                    'kategori_slug' => str_slug($request->kategori_name)
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Kategori Telah Diupdate');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Update Kategori');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = kategori::findOrFail(base64_decode($id));

        if ($del->delete()) {
            # code...
            Alert::success('Kategori Telah Dihapus!', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Kategori!', 'Error');
            return redirect()->back();
        }
    }
}
