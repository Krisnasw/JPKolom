<?php

namespace App\Http\Controllers;

use App\JenisIklan;
use App\Helpers;
use App\Media;
use Alert;
use Validator;
use Illuminate\Http\Request;

class IklanController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
        $med = Media::orderBy('media_id', 'desc')->get();
        view()->share('media', $med);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = JenisIklan::orderBy('iklan_id', 'desc')->get();
        return view('jenis.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('jenis.content', ['type' => 'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(),[
                'jenis' => 'required',
                'iklan_type' => 'required|string|max:50'
            ]);

        if ($valid->passes()) {
            # code...
            $q = JenisIklan::firstOrCreate([
                    'media_id' => $request->jenis,
                    'iklan_type' => $request->iklan_type,
                    'iklan_slug' => str_slug($request->iklan_type)
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Jenis Iklan Baru Berhasil Ditambahkan');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Tambah Jenis Iklan');
            return;
        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = JenisIklan::where('iklan_id', base64_decode($id))->first();
        return view('jenis.content', ['type' => 'edit', 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(),[
                'jenis' => 'required',
                'iklan_type' => 'required|string|max:50'
            ]);

        if ($valid->passes()) {
            # code...
            $q = JenisIklan::updateOrCreate(['iklan_id' => base64_decode($id)], [
                    'media_id' => $request->jenis,
                    'iklan_type' => $request->iklan_type,
                    'iklan_slug' => str_slug($request->iklan_type)
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Jenis Iklan Berhasil Diupdate');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Update Jenis Iklan');
            return;
        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = JenisIklan::findOrFail(base64_decode($id));

        if ($del->delete()) {
            # code...
            Alert::success('Jenis Iklan Telah Dihapus', 'Success!');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Jenis Iklan', 'Error!');
            return redirect()->back();
        }
    }
}
