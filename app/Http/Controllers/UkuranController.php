<?php

namespace App\Http\Controllers;

use App\Ukuran;
use App\Helpers;
use Alert;
use Validator;
use Illuminate\Http\Request;

class UkuranController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Ukuran::orderBy('ukuran_id', 'desc')->get();
        return view('ukuran.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('ukuran.content', ['type' => 'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
                'ukuran_kolom' => 'required|numeric',
                'ukuran_mm' => 'required|numeric'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Ukuran::firstOrCreate([
                    'ukuran_kolom' => $request->ukuran_kolom,
                    'ukuran_mm' => $request->ukuran_mm
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Ukuran Berhasil Ditambahkan');
                return;

            }

            echo Helpers::goResult(false, 'Gagal Menambah Ukuran');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Ukuran::where('ukuran_id', base64_decode($id))->first();
        return view('ukuran.content', ['data' => $data, 'type' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
                'ukuran_kolom' => 'required|numeric',
                'ukuran_mm' => 'required|numeric'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Ukuran::updateOrCreate(['ukuran_id' => base64_decode($id)], [
                    'ukuran_kolom' => $request->ukuran_kolom,
                    'ukuran_mm' => $request->ukuran_mm
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Ukuran Berhasil Ditambahkan');
                return;

            }

            echo Helpers::goResult(false, 'Gagal Menambah Ukuran');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Ukuran::findOrFail(base64_decode($id));

        if ($del->delete()) {
            # code...
            Alert::success('Ukuran Telah Dihapus', 'Success!');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Ukuran', 'Error!');
            return redirect()->back();
        }
    }
}
