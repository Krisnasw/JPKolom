<?php

namespace App\Http\Controllers;

use App\Paket;
use App\Kategori;
use App\Ukuran;
use App\Helpers;
use Alert;
use Validator;
use Illuminate\Http\Request;

class PaketController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
        $kat = Kategori::orderBy('kategori_id', 'desc')->get();
        $uk = Ukuran::orderBy('ukuran_id', 'desc')->get();
        view()->share(['kat' => $kat, 'uk' => $uk]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Paket::orderBy('paket_id', 'desc')->get();
        return view('paket.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('paket.content', ['type' => 'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
                'kategori_id' => 'required',
                'ukuran_id' => 'required',
                'paket_name' => 'required|string|max:100',
                'paket_color' => 'required|string|max:50',
                'paket_jml_tayang' => 'required|numeric',
                'paket_price' => 'required|numeric',
                'paket_area' => 'required|string|max:100',
                'paket_jml_huruf' => 'required|numeric',
                'paket_disabled_days' => 'required',
                'paket_active_days' => 'required',
                'paket_description' => 'required|string'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Paket::firstOrCreate([
                    'kategori_id' => $request->kategori_id,
                    'ukuran_id' => $request->ukuran_id,
                    'paket_name' => $request->paket_name,
                    'paket_slug' => str_slug($request->paket_name),
                    'paket_color' => $request->paket_color,
                    'paket_jml_tayang' => $request->paket_jml_tayang,
                    'paket_price' => $request->paket_price,
                    'paket_area' => $request->paket_area,
                    'paket_jml_huruf' => $request->paket_jml_huruf,
                    'paket_disabled_days' => implode(',', $request->paket_disabled_days),
                    'paket_active_days' => implode(',', $request->paket_active_days),
                    'paket_description' => $request->paket_description
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Paket Telah Ditambahkan');
                return;
            }

            echo Helpers::goResult(false, 'Paket Gagal Ditambahkan');
            return;
        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Paket::where('paket_id', base64_decode($id))->first();
        return view('paket.content', ['data' => $data, 'type' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
                'kategori_id' => 'required',
                'ukuran_id' => 'required',
                'paket_name' => 'required|string|max:100',
                'paket_color' => 'required|string|max:50',
                'paket_jml_tayang' => 'required|numeric',
                'paket_price' => 'required|numeric',
                'paket_area' => 'required|string|max:100',
                'paket_jml_huruf' => 'required|numeric',
                'paket_disabled_days' => 'required',
                'paket_active_days' => 'required',
                'paket_description' => 'required|string'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Paket::updateOrCreate(['paket_id' => base64_decode($id)], [
                    'kategori_id' => $request->kategori_id,
                    'ukuran_id' => $request->ukuran_id,
                    'paket_name' => $request->paket_name,
                    'paket_slug' => str_slug($request->paket_name),
                    'paket_color' => $request->paket_color,
                    'paket_jml_tayang' => $request->paket_jml_tayang,
                    'paket_price' => $request->paket_price,
                    'paket_area' => $request->paket_area,
                    'paket_jml_huruf' => $request->paket_jml_huruf,
                    'paket_disabled_days' => implode(',', $request->paket_disabled_days),
                    'paket_active_days' => implode(',', $request->paket_active_days),
                    'paket_description' => $request->paket_description
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Paket Telah Ditambahkan');
                return;
            }

            echo Helpers::goResult(false, 'Paket Gagal Ditambahkan');
            return;
        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Paket::findOrFail(base64_decode($id));

        if ($del->delete()) {
            # code...
            Alert::success('Paket Telah Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Paket', 'Error!');
            return redirect()->back();
        }
    }
}
