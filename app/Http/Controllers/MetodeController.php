<?php

namespace App\Http\Controllers;

use App\Metode;
use App\Helpers;
use Alert;
use Validator;
use Illuminate\Http\Request;

class MetodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Metode::orderBy('metode_id', 'desc')->get();
        return view('metode.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('metode.content', ['type' => 'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
                'metode_an' => 'required|string',
                'metode_bank' => 'required|string',
                'metode_norek' => 'required|numeric'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Metode::firstOrCreate([
                    'metode_an' => $request->metode_an,
                    'metode_bank' => $request->metode_bank,
                    'metode_norek' => $request->metode_norek
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Metode Berhasil Ditambahkan!');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Tambah Metode!');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Metode::where('metode_id', base64_decode($id))->first();
        return view('metode.content', ['type' => 'edit', 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (!$request->ajax()) {
            # code...
            exit('no direct scripts allowed');
        }

        $valid = Validator::make($request->all(), [
                'metode_an' => 'required|string',
                'metode_bank' => 'required|string',
                'metode_norek' => 'required|numeric'
            ]);

        if ($valid->passes()) {
            # code...
            $q = Metode::updateOrCreate(['metode_id' => base64_decode($id)], [
                    'metode_an' => $request->metode_an,
                    'metode_bank' => $request->metode_bank,
                    'metode_norek' => $request->metode_norek
                ]);

            if ($q) {
                # code...
                echo Helpers::goResult(true, 'Metode Berhasil Ditambahkan!');
                return;
            }

            echo Helpers::goResult(false, 'Gagal Tambah Metode!');
            return;

        }

        echo Helpers::goResult(false, 'Data Tidak Valid!');
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Metode::findOrFail(base64_decode($id));

        if ($del->delete()) {
            # code...
            Alert::success('Metode Telah Dihapus', 'Success!');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Metode', 'Error!');
            return redirect()->back();
        }
    }
}
