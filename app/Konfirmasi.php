<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konfirmasi extends Model
{
    //
    protected $table = 'konfirmasi';
    protected $primaryKey = 'konfirmasi_id';
    protected $fillable = ['order_no', 'k_pengirim', 'k_bank_pengirim', 'metode_id', 'k_nominal', 'k_tgl_transfer', 'k_tgl_approve', 'k_approve_by', 'bukti', 'customer_id'];

    public function metodes()
    {
    	return $this->belongsTo('App\Metode', 'metode_id');
    }

    public function users()
    {
    	return $this->belongsTo('App\User', 'customer_id' ,'id');
    }
}
