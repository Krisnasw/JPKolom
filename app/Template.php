<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    //
    protected $table = 'template';
    protected $primaryKey = 'template_id';
    protected $fillable = ['template_name', 'paket_id', 'template_design', 'ukuran_id'];
    public $timestamps = false;

    public function pakets()
    {
    	return $this->belongsTo('App\Paket', 'paket_id');
    }

    public function ukurans()
    {
    	return $this->belongsTo('App\Ukuran', 'ukuran_id');
    }
}
