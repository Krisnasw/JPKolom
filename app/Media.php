<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $table = 'media';
    protected $primaryKey = 'media_id';
    protected $fillable = ['media_name', 'media_slug'];
}
