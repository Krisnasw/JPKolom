<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    //
    protected $table = 'paket';
    protected $primaryKey = 'paket_id';
    protected $fillable = ['kategori_id', 'ukuran_id', 'paket_name', 'paket_slug', 'paket_color', 'paket_jml_tayang', 'paket_price', 'paket_area', 'paket_jml_huruf', 'paket_disabled_days', 'paket_active_days', 'paket_description'];
    public $timestamps = false;

    public function category()
    {
    	return $this->belongsTo('App\Kategori', 'kategori_id');
    }

    public function size()
    {
    	return $this->belongsTo('App\Ukuran', 'ukuran_id');
    }
}
