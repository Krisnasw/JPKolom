<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KonfirmasiMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('konfirmasi', function (Blueprint $table) {
            $table->increments('konfirmasi_id');
            $table->string('order_no', 50);
            $table->string('k_pengirim');
            $table->string('k_bank_pengirim');
            $table->string('k_bank_penerima');
            $table->decimal('k_nominal', 13,2);
            $table->date('k_tgl_transfer');
            $table->dateTime('k_tgl_approve');
            $table->string('k_approve_by', 100);
            $table->timestamps();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('konfirmasi');
    }
}
