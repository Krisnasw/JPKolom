<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('order', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('media_id');
            $table->integer('iklan_id');
            $table->integer('kategori_id');
            $table->integer('paket_id');
            $table->integer('customer_id');
            $table->integer('metode_id');
            $table->date('tgl_muat');
            $table->date('tgl_akhir');
            $table->string('order_no', 50)->unique();
            $table->string('total_biaya', 16);
            $table->string('judul_iklan', 100);
            $table->enum('status_iklan', ['paid', 'pending', 'canceled', 'overdue']);
            $table->text('description');
            $table->string('order_design');
            $table->date('order_due');
            $table->timestamps();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('order');
    }
}
