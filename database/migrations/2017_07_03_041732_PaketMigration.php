<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaketMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('paket', function (Blueprint $table) {
            $table->increments('paket_id');
            $table->integer('kategori_id');
            $table->integer('ukuran_id');
            $table->string('paket_name');
            $table->string('paket_slug');
            $table->string('paket_color');
            $table->integer('paket_jml_tayang');
            $table->integer('paket_price');
            $table->string('paket_area');
            $table->string('paket_jml_huruf');
            $table->string('paket_disabled_days');
            $table->string('paket_active_days');
            $table->text('paket_description');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('paket');
    }
}
