var customTshirt = function customTshirt() {
    var canvas,
        a,
        b;

    canvas = new fabric.Canvas('tcanvas', {
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor: 'blue'
    });
    canvas.on({
        'object:moving': function(e) {
            e.target.opacity = 0.5
        },
        'object:modified': function(e) {
            e.target.opacity = 1
        },
        'object:selected': onObjectSelected,
        'selection:cleared': onSelectedCleared
    });
    canvas.findTarget = (function(b) {
        return function() {
            var a = b.apply(this, arguments);
            if (a) {
                if (this._hoveredTarget !== a) {
                    canvas.fire('object:over', {
                        target: a
                    });
                    if (this._hoveredTarget) {
                        canvas.fire('object:out', {
                            target: this._hoveredTarget
                        })
                    }
                    this._hoveredTarget = a
                }
            } else if (this._hoveredTarget) {
                canvas.fire('object:out', {
                    target: this._hoveredTarget
                });
                this._hoveredTarget = null
            }
            return a
        }
    })(canvas.findTarget);
    canvas.setBackgroundImage('../../images/placeholder/placeholder.png', canvas.renderAll.bind(canvas), {
        backgroundImageStretch: false
    });
    canvas.on('object:over', function(e) {});
    canvas.on('object:out', function(e) {});
    document.getElementById('add-text').onclick = function() {
        var a = $("#text-string").val();
        var b = new fabric.CurvedText(a, {
            left: fabric.util.getRandomInt(0, 200),
            top: fabric.util.getRandomInt(0, 400),
            fontFamily: 'helvetica',
            angle: 0,
            fill: '#000000',
            scaleX: 0.5,
            scaleY: 0.5,
			effect: $("#effect").val(),
            fontWeight: '',
            hasRotatingPoint: true
        });
        canvas.add(b);
        canvas.item(canvas.item.length - 1).hasRotatingPoint = true;
		if($("#effect").val() == 'curved' || $("#effect").val() == 'arc'){	
			$("#curverdText").css('display', 'block');
		}else{			
			$("#curverdText").css('display', 'none');
		}
        $("#texteditor").css('display', 'block');
        $("#imageeditor").css('display', 'block')
    };
	$('#radius, #spacing, #effect').change(function(){
		var obj = canvas.getActiveObject();
		if(obj){
			obj.set($(this).attr('id'),$(this).val()); 
			if($('#effect').val() == 'curved' || $('#effect').val() == 'arc'){	
				$("#curverdText").css('display', 'block');
			}else{			
				$("#curverdText").css('display', 'none');
			}
		}
		canvas.renderAll();
	});
	$('#reverse').click(function(){
		var obj = canvas.getActiveObject();
		if(obj){
			obj.set('reverse',$(this).is(':checked')); 
			canvas.renderAll();
		}
	});
    $("#text-string").keyup(function() {
        var a = canvas.getActiveObject();
        if (a && a.type === 'curvedText') {
			a.setText(this.value); 
            canvas.renderAll()
        }
    });
    $(".tfont").click(function() {
        var a = canvas.getActiveObject();
        if (a && a.type === 'curvedText') {
			a.set('fontFamily', $(this).html()); 
            canvas.renderAll();
        }
		return false;
    });
    document.getElementById('remove-selected').onclick = function() {
        var b = canvas.getActiveObject(),
            activeGroup = canvas.getActiveGroup();
        if (b) {
            canvas.remove(b);
            $("#text-string").val("")
        } else if (activeGroup) {
            var c = activeGroup.getObjects();
            canvas.discardActiveGroup();
            c.forEach(function(a) {
                canvas.remove(a)
            })
        }
    };
    document.getElementById('bring-to-front').onclick = function() {
        var b = canvas.getActiveObject(),
            activeGroup = canvas.getActiveGroup();
        if (b) {
            canvas.bringToFront(b)
        } else if (activeGroup) {
            var c = activeGroup.getObjects();
            canvas.discardActiveGroup();
            c.forEach(function(a) {
                canvas.bringToFront(a)
            })
        }
    };
    document.getElementById('send-to-back').onclick = function() {
        var b = canvas.getActiveObject(),
            activeGroup = canvas.getActiveGroup();
        if (b) {
             canvas.sendToBack(b)
        } else if (activeGroup) {
            var c = activeGroup.getObjects();
            canvas.discardActiveGroup();
            c.forEach(function(a) {
                canvas.sendToBack(a)
            })
        }
    };
    $("#text-bold").click(function() {
        var a = canvas.getActiveObject();
        if (a && a.type === 'curvedText') {
			a.set('fontWeight', (a.fontWeight == 'bold' ? '' : 'bold')); 
            canvas.renderAll()
        }
    });
    $("#text-italic").click(function() {
        var a = canvas.getActiveObject();
        if (a && a.type === 'curvedText') {
			a.set('fontStyle', (a.fontStyle == 'italic' ? '' : 'italic')); 
            canvas.renderAll()
        }
    });
    $("#text-strike").click(function() {
        var a = canvas.getActiveObject();
        if (a && a.type === 'curvedText') {
			a.set('textDecoration', (a.textDecoration == 'line-through' ? '' : 'line-through')); 
            canvas.renderAll()
        }
    });
    $("#text-underline").click(function() {
        var a = canvas.getActiveObject();
        if (a && a.type === 'curvedText') {
			a.set('textDecoration', (a.textDecoration == 'underline' ? '' : 'underline')); 
            canvas.renderAll()
        }
    });
    $('#ts-bcolor').miniColors({
        change: function(a, b) {
            canvas.backgroundColor = this.value;
            canvas.renderAll();
        },
        open: function(a, b) {},
        close: function(a, b) {}
    });
    $('#text-bgcolor').miniColors({
        change: function(a, b) {
            var c = canvas.getActiveObject();
            if (c && c.type === 'curvedText') {
				c.set('backgroundColor', this.value);
                canvas.renderAll()
            }
        },
        open: function(a, b) {},
        close: function(a, b) {}
    });
    $('#text-fontcolor').miniColors({
        change: function(a, b) {
            var c = canvas.getActiveObject();
            if (c && c.type === 'curvedText') {
				c.set('fill', this.value);
                canvas.renderAll()
            }
        },
        open: function(a, b) {},
        close: function(a, b) {}
    });
    $('#text-strokecolor').miniColors({
        change: function(a, b) {
            var c = canvas.getActiveObject();
            if (c && c.type === 'curvedText') {
				c.set('stroke', this.value);
                canvas.renderAll()
            }
        },
        open: function(a, b) {},
        close: function(a, b) {}
    });
    // $("#tshirtTypes").change(function(e) {
    //     canvas.setBackgroundImage('../assets/screenshot/src/img/t-shirt/' + $(this).val() + '_front.png', canvas.renderAll.bind(canvas), {
    //         backgroundImageStretch: false
    //     });
    // });
    $('#flip').click(function() {
        if ($(this).attr("data-original-title") == "Show Back View") {
            $(this).attr('data-original-title', 'Show Front View');
            canvas.setBackgroundImage('../assets/screenshot/src/img/t-shirt/' + $('#tshirtTypes').val() + '_back.png', canvas.renderAll.bind(canvas), {
                backgroundImageStretch: false
            });
        } else {
            $(this).attr('data-original-title', 'Show Back View');
            canvas.setBackgroundImage('../assets/screenshot/src/img/t-shirt/' + $('#tshirtTypes').val() + '_front.png', canvas.renderAll.bind(canvas), {
                backgroundImageStretch: false
            });
        }
        canvas.renderAll();
    });

    document.getElementById('inputImg').addEventListener('change', readFile, false);

    function getRandomNum(min, max) {
        return Math.random() * (max - min) + min;
    }

    function onObjectSelected(e) {
        var selectedObject = e.target;
        $("#text-string").val("");
        selectedObject.hasRotatingPoint = true
        if (selectedObject && selectedObject.type === 'curvedText') {
            //display text editor	    	
            $("#texteditor").css('display', 'block');
            $("#text-string").val(selectedObject.getText());
            $('#text-bgcolor').miniColors('value', selectedObject.backgroundColor);
            $('#text-fontcolor').miniColors('value', selectedObject.fill);
            $('#text-strokecolor').miniColors('value', selectedObject.stroke);
            $("#imageeditor").css('display', 'block');
			$('#effect').val(selectedObject.getEffect());	
			if(selectedObject.getEffect() == 'curved' || selectedObject.getEffect() == 'arc'){
				$('#reverse').prop('checked', selectedObject.get('reverse'));
				$('#radius').val(selectedObject.get('radius'));
				$('#spacing').val(selectedObject.get('spacing'));
				$("#curverdText").css('display', 'block');
			}else{			
				$("#curverdText").css('display', 'none');
			}
        } else if (selectedObject && selectedObject.type === 'image') {
            //display image editor
            $("#texteditor").css('display', 'none');
            $("#imageeditor").css('display', 'block');
        }
    }

    function onSelectedCleared(e) {
        $("#texteditor").css('display', 'none');
        $("#text-string").val("");
        $("#imageeditor").css('display', 'none');
    }

    function removeWhite() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && activeObject.type === 'image') {
            activeObject.filters[2] = new fabric.Image.filters.RemoveWhite({
                hreshold: 100,
                distance: 10
            }); //0-255, 0-255
            activeObject.applyFilters(canvas.renderAll.bind(canvas));
        }
    }

    function readFile(evt) {
        var f = evt.target.files[0];
        if (f) {
            if (/(jpe?g|png|gif)$/i.test(f.type)) {
                var r = new FileReader();
                r.onload = function(e) {
                    var base64Img = e.target.result;
                    var offset = 50;
                    var left = fabric.util.getRandomInt(0 + offset, 200 - offset);
                    var top = fabric.util.getRandomInt(0 + offset, 400 - offset);
                    var angle = fabric.util.getRandomInt(-20, 40);
                    var width = fabric.util.getRandomInt(30, 50);
                    var opacity = (function(min, max) {
                        return Math.random() * (max - min) + min;
                    })(0.5, 1);
                    fabric.Image.fromURL(base64Img, function(image) {
                        image.set({
                            left: left,
                            top: top,
                            angle: 0,
                            padding: 10,
                            cornersize: 10,
                            hasRotatingPoint: true
                        });
                        canvas.add(image);
                    });
                }
                r.readAsDataURL(f);
            } else {
                alert("Failed file type");
            }
        } else {
            alert("Failed to load file");
        }
    }
	
	function convertDataURIToBinary(dataURI) {
		var BASE64_MARKER = ';base64,';
		var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
		var base64 = dataURI.substring(base64Index);
		var raw = window.atob(base64);
		var rawLength = raw.length;
		var array = new Uint8Array(new ArrayBuffer(rawLength));

		for(i = 0; i < rawLength; i++) {
			array[i] = raw.charCodeAt(i);
		}
		return array;
	}

	var downloadFile = (function () {
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		return function () {
			var data = convertDataURIToBinary(canvas.toDataURL('png')), 
				fileName = 'image.png',
				blob = new Blob([data], {type: 'image/png'}),
				url = window.URL.createObjectURL(blob);
			a.href = url;
			a.download = fileName;
			a.click();
			window.URL.revokeObjectURL(url);
		};
	}());
	document.getElementById('downloadImg').addEventListener('click', downloadFile, false);

}