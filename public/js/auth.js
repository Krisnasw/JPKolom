$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#form-login").submit(function(e) {
	e.preventDefault();
	var block = $("#form-login > div");
	
	$.ajax({
		url	: '/mvi-admin/api/login',
		method : 'POST',
		data : $("#form-login").serialize(),
		beforesend:function() {
			showBlock(block,'<i class="icon-spinner4 spinner"></i>','#fff');		
		}
	})
	.done(function(response){
		data = JSON.parse(response);
		if (data.status == false) {

			toastr.error(data.msg, 'Login Failed!')
			$("#form-login").find('input').val('');
			$(block).unblock();
			return;
			
		}

		toastr.success(data.msg, 'Login success!');
		$(block).unblock();

		var go	= setTimeout(function() {
			window.location.href = "/mvi-admin/home";
		},1000);

	})
});

$("#form-register").submit(function(e) {
	e.preventDefault();
	var block = $("#form-register > div");
	
	$.ajax({
		url	: '/mvi-admin/api/register',
		method : 'POST',
		data : $("#form-register").serialize(),
		beforesend:function() {
			showBlock(block,'<i class="icon-spinner4 spinner"></i>','#fff');		
		}
	})
	.done(function(response){
		data = JSON.parse(response);
		if (data.status == false) {

			toastr.error(data.msg, 'Register Failed!')
			$("#form-register").find('input').val('');
			$(block).unblock();
			return;
			
		}

		toastr.success(data.msg, 'Register success!');
		$(block).unblock();

		var go	= setTimeout(function() {
			window.location.href = "/";
		},1000);

	})
});

$("#form-recovery").submit(function(e) {
	e.preventDefault();
	var block = $("#form-recovery > div");
	
	$.ajax({
		url	: '/mvi-admin/api/recovery',
		method : 'POST',
		data : $("#form-recovery").serialize(),
		beforesend:function() {
			showBlock(block,'<i class="icon-spinner4 spinner"></i>','#fff');		
		}
	})
	.done(function(response){
		data = JSON.parse(response);
		if (data.status == false) {

			toastr.error(data.msg, 'Lupa Password Gagal!');
			$("#form-register").find('input').val('');
			$(block).unblock();
			return;
			
		}

		toastr.success(data.msg, 'Forget Password success!');
		$(block).unblock();

		var go	= setTimeout(function() {
			window.location.href = "/";
		},1000);

	})
});