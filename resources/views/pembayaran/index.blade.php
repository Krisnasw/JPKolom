@extends('index')

@section('title')
Advertisement Information System - History Pembayaran
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/m/history-pembayaran') }}"><i class="icon-stack2 position-left"></i> Data History Pembayaran</a></li>
			<li class="active">List History Pembayaran</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> History</span> Pembayaran
		<small class="display-block">Ini Merupakan History Pembayaran Yang Telah Dibuat</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">History Pembayaran Anda</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>No. Order</th>
                            <th>Nama Pengirim</th>
                            <th>Bank Pengirim</th>
                            <th>Nominal</th>
                            <th>Bank Tujuan</th>
                            <th>Di Konfirmasi Oleh</th>
                            <th>Di Konfirmasi Tanggal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($data as $key => $result)
                         <tr>
                        	<td align="left" style="width: 15%;">{!! $result->order_no !!}</td>
	                        <td align="left">{!! $result->k_pengirim !!}</td>
	                        <td align="left">{!! $result->k_bank_pengirim !!}</td>
	                        <td align="left">{!! $result->k_nominal !!}</td>
	                        <td align="left">{!! $result->metodes->metode_bank !!}</td>
	                        <td align="left">
	                        	{!! ($result->k_approve_by != null) ? $result->k_approve_by : 'Menunggu Konfirmasi' !!}
	                        </td>
	                        <td align="left">
	                        	{!! ($result->k_tgl_approve != null) ? $result->k_tgl_approve : 'Menunggu Konfirmasi' !!}
	                        </td>
	                        <td>
	                        	<div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
			                    		<li>
			                    			<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_detail{{{ $result->konfirmasi_id }}}">
			                    				<i class="icon-eye"></i> Detail Pembayaran
			                    			</a>
			                    		</li>
			                    		<li>
			                    			<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_konfirm{{{ $result->konfirmasi_id }}}">
			                    				<i class="icon-stamp"></i> Konfirmasi Pembayaran
			                    			</a>
			                    		</li>
									</ul>
								</div>
	                        </td>
                        </tr>

                        <!-- Danger modal -->
						<div id="modal_detail{!! $result->konfirmasi_id !!}" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-success">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h6 class="modal-title">Detail Pembayaran</h6>
									</div>

									<div class="modal-body">
										<h6 class="text-semibold"> No Order : <i> {!! $result->order_no !!} </i></h6>
										<h6 class="text"> Nama Customer : {!! $result->users->name !!} </h6>
										<h6 class="text"> Nama Pengirim : {!! $result->k_pengirim !!} </h6>
										<h6 class="text"> Bank Pengirim : {!! $result->k_bank_pengirim !!} </h6>
										<h6 class="text"> Bank Tujuan : {!! $result->metodes->metode_bank !!} - An. {!! $result->metodes->metode_an !!} </h6>
										<h6 class="text"> Nominal Transfer : {!! number_format($result->k_nominal) !!} </h6>
										<h6 class="text"> Tanggal Transfer : {!! Helpers::tgl_indo($result->k_tgl_transfer) !!} </h6>
										<center>
											<span type="text"> Bukti Transfer Dibawah Ini </span>
											<br />
											<img class="img img-responsive" src="{{ asset($result->bukti) }}">
										</center>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->

                        <!-- Danger modal -->
						<div id="modal_konfirm{!! $result->konfirmasi_id !!}" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-success">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h6 class="modal-title">Konfirmasi Order</h6>
									</div>

									<div class="modal-body">
										<h6 class="text-semibold">Apakah Anda Yakin Akan Mengkonfirmasi Pembayaran Order No : <i> {!! $result->order_no !!} </i></h6>
									</div>

									<div class="modal-footer">
										{!! Form::open(['method' => 'PATCH', 'action' => ['PaymentController@update', base64_encode($result->konfirmasi_id)]]) !!}
			                            	{!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
			                            {!! Form::close() !!}
										<button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/media/fancybox.min.js') !!}
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
{!! Html::script('admin_assets/js/plugins/tables/datatables/datatables.min.js') !!}
{!! Html::script('admin_assets/js/pages/gallery_library.js') !!}
@endsection