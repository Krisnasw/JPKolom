<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  {!! SEO::generate(true) !!}
  <link rel="icon" href="">
  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="{{ asset('admin_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('admin_assets/css/minified/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('admin_assets/css/minified/core.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('admin_assets/css/minified/components.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('admin_assets/css/minified/colors.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('admin_assets/css/extras/animate.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <!-- /global stylesheets -->

</head>

<body>

  <!-- Page container -->
  <div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper" style="background-image: url({{ asset('images/bg.jpg') }}); background-size: cover;">

        <!-- Content area -->
        <div class="content">

          <!-- Simple login form -->
          {!! Form::open(['method' => 'POST', 'id' => 'form-login']) !!}
            <div class="panel panel-body login-form animated tada ">
              <div class="text-center">
                <div class="icon-object border-primary-400 text-primary-400"><i class="icon-user-tie"></i></div>
                <h5 class="content-group">Advertisement Information System</h5>
              </div>

              <div class="form-group has-feedback has-feedback-left">
                <input type="username" class="form-control" name="username" placeholder="Username" required>
                <div class="form-control-feedback">
                  <i class="icon-user text-muted"></i>
                </div>
              </div>

              <div class="form-group has-feedback has-feedback-left">
                <input type="password" class="form-control"  name="password" placeholder="Password" required>
                <div class="form-control-feedback">
                  <i class="icon-lock2 text-muted"></i>
                </div>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Masuk <i class="icon-circle-right2 position-right"></i></button>
              </div>

              <div class="text-center">
                <a href="{{ url('/register') }}">Belum Punya Akun? Daftar Disini</a>
              </div>

              <div class="text-center">
                <span>Atau</span>
              </div>

              <div class="text-center">
                <a href="{{ url('/forget-password') }}">Lupa Password? Klik Disini</a>
              </div>
            </div>
          {!! Form::close() !!}

          <!-- Footer -->
          <div class="footer text-muted">
            <p style="color: #000;">&copy; 2017&nbsp;<a href="https://www.mvi.web.id/" style="color: #000;" target="_blank"> PT Media Virtual Indonesia - All Rights Reserved</a></p>
          </div>
          <!-- /footer -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->

  <!-- Core JS files -->
  <script type="text/javascript" src="{{ asset('admin_assets/js/plugins/loaders/pace.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/core/libraries/jquery.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/core/libraries/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/plugins/loaders/blockui.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/plugins/notifications/noty.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/plugins/ui/prism.min.js') }}"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="{{ asset('admin_assets/js/core/app.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/pages/components_notifications_other.js') }}"></script>
  <script type="text/javascript" src="{{ asset('admin_assets/js/pages/extension_blockui.js') }}"></script>
  <!-- /theme JS files -->

  <script type="text/javascript" src="{{ asset('admin_assets/js/uprak.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/auth.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
</body>
</html>