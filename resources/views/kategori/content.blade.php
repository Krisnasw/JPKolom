@extends('index')

@section('title')
{{ ($type == "create") ? 'Buat Kategori Baru' : 'Edit Kategori' }} - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/kategori') }}"><i class="icon-stack2 position-left"></i> Kategori</a></li>
			<li class="active">Data Kategori</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type == "create") ? 'Buat' : 'Update' }}</span> Kategori
		<small class="display-block">
			{{ ($type == "create") ? 'Buat Kategori Baru' : 'Perbarui Kategori' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Kategori</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
				@if($type == "create")
					{!! Form::open(['id' => 'form-kategori', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'KategoriController@store', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Iklan" class="select" name="jenis">
										<option value="selected">Pilih Jenis Iklan</option>
										@foreach($iklan as $ik => $key)
											<option value="{!! $key->iklan_id !!}">{!! $key->iklan_type !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Kategori <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Kategori" name="kategori_name" 
								value="{{ ($type=='create') ? '' : $data->kategori_name }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Kategori' : 'Ubah Kategori' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@else
					{!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['KategoriController@update', base64_encode($data->kategori_id)], 'files' => true, 'id' => 'form-kategori']) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Iklan" class="select" name="jenis">
										<option value="selected">Pilih Jenis Iklan</option>
										@foreach($iklan as $ik => $key)
											<option value="{!! $key->iklan_id !!}">{!! $key->iklan_type !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Kategori <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Kategori" name="kategori_name" 
								value="{{ ($type=='create') ? '' : $data->kategori_name }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Kategori' : 'Ubah Kategori' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@endif
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script>
  (function($){
    $(function() {
      $("#email").emailautocomplete({
        domains: ["example.com"] //add your own domains
      });
    });
  }(jQuery));
</script>

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ ($type=='create') ? Helpers::img_holder() : asset($data->image) }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-kategori").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-kategori")[0] );

			$.ajax({
				url: 		$("#form-kategori").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-kategori'),'Please Wait , {{ ($type =="create") ? "Menambahkan Kategori" : "Memperbarui Kategori" }}','#fff');
				}
			})
			.done(function(response) {
				$('#form-kategori').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : '{{ ($type =="create") ? "Kategori Berhasil Dibuat" : "Kategori Telah Diperbaharui" }}'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/kategori') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-kategori').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

{!! Html::script('admin_assets/js/pages/form_select2.js') !!}
@endsection