@extends('index')

@section('title')
Advertisement Information System - Data Cancel Order Customer
@endsection

@section('content')
<!-- Page header -->
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Advertisement Information System</span> - Data Cancel Order Customer</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Data Cancel Order Customer</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> Daftar</span> Cancel Order Customer
		<small class="display-block">Ini Merupakan Data Cancel Order Customer Yang Telah Dibuat</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Cancel Order Customer</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>Tanggal Pesan</th>
                            <th>Judul Iklan</th>
                            <th>Jenis Iklan</th>
                            <th>Biaya</th>
                            <th>Nama Pemesan</th>
                            <th>Status Order</th>
                            <th>Bukti Pembayaran</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($data as $key => $result)
                         <tr>
                        	<td align="left" style="width: 15%;">{!! $result->order_no !!}</td>
	                        <td align="left">{!! $result->judul_iklan !!}</td>
	                        <td align="left">{!! $result->medias->media_name !!}</td>
	                        <td align="left">{!! $result->total_biaya !!}</td>
	                        <td align="left"><i><b>{!! $result->users->name !!}</b></i></td>
	                      	<td>
	                      		@if($result->status_iklan == 'paid')
	                      			<label class="label label-success">PAID</label>
	                      		@elseif($result->status_iklan == 'pending')
	                      			<label class="label label-info">PENDING</label>
	                      		@elseif($result->status_iklan == 'overdue')
	                      			<label class="label label-warning">OVERDUE</label>
	                      		@else
	                      			<label class="label label-danger">CANCELED</label>
	                      		@endif
	                      	</td>
	                      	<td>{!! Helpers::tgl_indo($result->order_due) !!}</td>
	                        <td class="text-center">
	                           <div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
			                    		<li>
			                    			<a href="{{ url('/mvi-admin/order') }}/{!! base64_encode($result->order_id) !!}">
			                    				<i class="icon-eye"></i> Detail Order
			                    			</a>
			                    		</li>
									</ul>
								</div>
	                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/media/fancybox.min.js') !!}
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
{!! Html::script('admin_assets/js/plugins/tables/datatables/datatables.min.js') !!}
{!! Html::script('admin_assets/js/pages/gallery_library.js') !!}
@endsection