@extends('index')

@section('title')
Advertisement Information System - Daftar Order
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/m/list-order') }}"><i class="icon-stack2 position-left"></i> Data Order</a></li>
			<li class="active">List Order</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> Daftar</span> Order
		<small class="display-block">Ini merupakan daftar Order yang telah dibuat</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Daftar Order Anda</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>No. Order</th>
                            <th>Judul Iklan</th>
                            <th>Media Iklan</th>
                            <th>Biaya</th>
                            <th>Tanggal Tayang</th>
                            <th>Status Order</th>
                            <th>Batas Pembayaran</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($data as $key => $result)
                         <tr>
                        	<td align="left" style="width: 15%;">{!! $result->order_no !!}</td>
	                        <td align="left">{!! $result->judul_iklan !!}</td>
	                        <td align="left">{!! $result->medias->media_name !!}</td>
	                        <td align="left">{!! $result->total_biaya !!}</td>
	                        <td align="left">{!! Helpers::tgl_indo($result->tgl_muat) !!}</td>
	                      	<td>
	                      		@if($result->status_iklan == 'paid')
	                      			<label class="label label-success">PAID</label>
	                      		@elseif($result->status_iklan == 'pending')
	                      			<label class="label label-info">PENDING</label>
	                      		@elseif($result->status_iklan == 'overdue')
	                      			<label class="label label-warning">OVERDUE</label>
	                      		@else
	                      			<label class="label label-danger">CANCELED</label>
	                      		@endif
	                      	</td>
	                      	<td>{!! Helpers::tgl_indo($result->order_due) !!}</td>
	                        <td class="text-center">
	                           <div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
			                    		<li>
			                    			<a href="{{ url('/mvi-admin/m/show-order') }}/{!! base64_encode($result->order_id) !!}/show">
			                    				<i class="icon-eye"></i> Detail Order
			                    			</a>
			                    		</li>
										<li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_theme_danger{{{ $result->order_id }}}">
												<i class="icon-trash"></i> Cancel Order
											</a>
										</li>
									</ul>
								</div>
	                        </td>
                        </tr>

                        <!-- Danger modal -->
						<div id="modal_theme_danger{{{ $result->order_id }}}" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-danger">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h6 class="modal-title">Cancel Order</h6>
									</div>

									<div class="modal-body">
										<h6 class="text-semibold">Apakah Anda Yakin Membatalkan Order <i>No. Order : {!! $result->order_no !!} </i></h6>
									</div>

									<div class="modal-footer">
										{!! Form::open(['method' => 'GET', 'action' => ['AuthController@canceledOrder', base64_encode($result->order_id)]]) !!}
			                            	{!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
			                            {!! Form::close() !!}
										<button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/media/fancybox.min.js') !!}
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
{!! Html::script('admin_assets/js/plugins/tables/datatables/datatables.min.js') !!}
{!! Html::script('admin_assets/js/pages/gallery_library.js') !!}
@endsection