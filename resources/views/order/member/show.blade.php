@extends('index')

@section('title')
Advertisement Information System - Detail Order
@endsection

@section('content')
<!-- Page header -->
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Advertisement Information System</span> - Detail Order</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{ url('/mvi-admin/m/list-order') }}">Order</a></li>
			<li class="active">Detail Order</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<div class="content">
@foreach($data as $val => $key)
	<!-- Invoice template -->
	<div class="panel panel-white">
		<div class="panel-heading">
			<h6 class="panel-title">Jawa Pos Invoice</h6>
			<div class="heading-elements">
				<a href="{{ url('/mvi-admin/export/pdf') }}/{!! base64_encode($key->order_id) !!}" class="btn btn-default btn-xs heading-btn"><i class="icon-file-check position-left"></i> Save</a>
				<button type="button" class="btn btn-default btn-xs heading-btn" onclick="window.print();"><i class="icon-printer position-left"></i> Print</button>
			</div>
		</div>
		<div class="panel-body no-padding-bottom">
			<div class="row">
				<div class="col-md-6 content-group">
					<img src="{{ asset('images/website/logo/logo.png') }}" class="content-group mt-10" alt="" style="width: 120px;">
			 		<ul class="list-condensed list-unstyled">
						<li>Jalan Jenderal A. Yani No. 88 Ketintang Gayungan</li>
						<li>Surabaya, Indonesia</li>
						<li>+6221 53699605</li>
					</ul>
				</div>

				<div class="col-md-6 content-group">
					<div class="invoice-details">
						<h5 class="text-uppercase text-semibold">Invoice - {!! $key->order_no !!}</h5>
						<ul class="list-condensed list-unstyled">
							<li>Tanggal Pesan: <span class="text-semibold">{!! Helpers::tgl_indo($key->created_at) !!}</span></li>
							<li>Batas Pembayaran: <span class="text-semibold">{!! Helpers::tgl_indo($key->order_due) !!}</span></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-9 content-group">
					<span class="text-muted">Ditujukan Kepada:</span>
				 	<ul class="list-condensed list-unstyled">
						<li><h5>{!! $key->users->name !!}</h5></li>
						<li>{!! $key->users->address !!}</li>
						<li>{!! $key->users->contact !!}</li>
						<li><a href="mailto:{!! $key->users->email !!}">{!! $key->users->email !!}</a></li>
					</ul>
				</div>

				<div class="col-md-6 col-lg-3 content-group">
					<span class="text-muted">Detail Pembayaran:</span>
					<ul class="list-condensed list-unstyled invoice-payment-details">
						<li><h5>Total Biaya: <span class="text-right text-semibold">Rp.{!! number_format($key->total_biaya) !!}</span></h5></li>
						<li>Bank name: <span class="text-semibold">{!! $key->metodes->metode_bank !!}</span></li>
						<li>Atas Nama : <span>{!! $key->metodes->metode_an !!}</span></li>
						<li>No. Rekening: <span>{!! $key->metodes->metode_norek !!}</span></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="table-responsive">
			<table class="table table-lg">
				<thead>
					<tr>
						<th>Deskripsi Item</th>
						<th class="col-sm-1">Media</th>
						<th class="col-sm-1">Jenis Iklan</th>
						<th class="col-sm-1">Kategori</th>
						<th class="col-sm-1">Paket</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<h6 class="no-margin">{!! $key->judul_iklan !!}</h6>
							<span class="text-muted">{!! $key->description !!}</span>
						</td>
						<td>{!! $key->medias->media_name !!}</td>
						<td>{!! $key->jenis->iklan_type !!}</td>
						<td>{!! $key->categories->kategori_name !!}</td>
						<td>{!! $key->pakets->paket_name !!}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="panel-body">
			<div class="row invoice-payment">
				<div class="col-sm-7">
					<div class="content-group">
						<h6>Design Iklan</h6>
						<div class="mb-15 mt-15">
							<img src="{{ asset($key->order_design) }}" class="display-block" style="width: 150px;" alt="">
						</div>
					</div>
				</div>

				<div class="col-sm-5">
					<div class="content-group">
						<h6>Total Biaya</h6>
						<div class="table-responsive no-border">
							<table class="table">
								<tbody>
									<tr>
										<th>Subtotal:</th>
										<td class="text-right">Rp.{!! number_format($key->harga_asli) !!}</td>
									</tr>
									<tr>
										<th>PPN: <span class="text-regular">(10%)</span></th>
										<td class="text-right">Rp.{!! number_format($key->ppn) !!}</td>
									</tr>
									<tr>
										<th>Total:</th>
										<td class="text-right text-primary"><h5 class="text-semibold">Rp.{!! number_format($key->total_biaya) !!}</h5></td>
									</tr>
								</tbody>
							</table>
						</div>

						{{-- <div class="text-right">
							<button type="button" class="btn btn-primary btn-labeled"><b><i class="icon-paperplane"></i></b> Blah</button>
						</div> --}}
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- /invoice template -->
@endforeach
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/pages/invoice_template.js') !!}
{!! Html::script('admin_assets/js/core/app.js') !!}
@endsection