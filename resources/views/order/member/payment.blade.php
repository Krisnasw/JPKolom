@extends('index')

@section('title')
Konfirmasi Pembayaran - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/pasang-iklan') }}"><i class="icon-stack2 position-left"></i> Konfirmasi Pembayaran</a></li>
			<li class="active">Konfirmasi Pembayaran</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> Konfirmasi Pembayaran</span> Advertisement Information System
		<small class="display-block">
			Konfirmasi Pembayaran
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Konfirmasi Pembayaran</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
					{!! Form::open(['id' => 'form-media', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'AuthController@postBukti', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">No. Order <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" name="no_order" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Pengirim <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" name="name_p" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Bank Pengirim <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" name="bank_p" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Bank Penerima <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Bank Penerima" class="select" name="b_penerima" id="p_paket">
									@foreach($metod as $data => $key)
										<option value="{!! $key->metode_id !!}">{!! $key->metode_bank !!} - An. {!! $key->metode_an !!}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nominal Transfer <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="number" name="nominal" required min="0">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Tanggal Transfer <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-5">
								<div class="input-group" id="tgl_awal">
									<span class="input-group-addon"><i class="icon-calendar5"></i></span>
									<input type="date" class="form-control pickadate-year" placeholder="Try me&hellip;" name="tgl_tf">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Bukti Pembayaran<span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="file" class="file-input-custom" name="image" accept="image/*">
									<span class="help-block">  
									NB : Ukuran Maksimum 2MB , Hanya JPG, JPEG, PNG
									</span>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
							<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
						</div>
					{!! Form::close() !!}
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ Helpers::img_holder() }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-media").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-media")[0] );

			$.ajax({
				url: 		$("#form-media").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-media'),'Please Wait , Memproses Order','#fff');
				}
			})
			.done(function(response) {
				$('#form-media').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : 'Order Berhasil Silahkan Melunasi Pesanan Anda'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/m/history-pembayaran') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-media').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

@endsection