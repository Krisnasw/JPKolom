@extends('index')

@section('title')
Advertisement Information System - History Pembayaran
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/m/history-pembayaran') }}"><i class="icon-stack2 position-left"></i> Data History Pembayaran</a></li>
			<li class="active">List History Pembayaran</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> History</span> Pembayaran
		<small class="display-block">Ini Merupakan History Pembayaran Yang Telah Dibuat</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">History Pembayaran Anda</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>No. Order</th>
                            <th>Nama Pengirim</th>
                            <th>Bank Pengirim</th>
                            <th>Nominal</th>
                            <th>Bank Tujuan</th>
                            <th>Di Konfirmasi Oleh</th>
                            <th>Di Konfirmasi Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($data as $key => $result)
                         <tr>
                        	<td align="left" style="width: 15%;">{!! $result->order_no !!}</td>
	                        <td align="left">{!! $result->k_pengirim !!}</td>
	                        <td align="left">{!! $result->k_bank_pengirim !!}</td>
	                        <td align="left">{!! $result->k_nominal !!}</td>
	                        <td align="left">{!! $result->metodes->metode_bank !!}</td>
	                        <td align="left">
	                        	{!! ($result->k_approve_by != null) ? $result->k_approve_by : 'Menunggu Konfirmasi' !!}
	                        </td>
	                        <td align="left">
	                        	{!! ($result->k_tgl_approve != null) ? $result->k_tgl_approve : 'Menunggu Konfirmasi' !!}
	                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/media/fancybox.min.js') !!}
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
{!! Html::script('admin_assets/js/plugins/tables/datatables/datatables.min.js') !!}
{!! Html::script('admin_assets/js/pages/gallery_library.js') !!}
@endsection