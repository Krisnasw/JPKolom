@extends('index')

@section('title')
Advertisement Information System - Data Order Customer
@endsection

@section('content')
<!-- Page header -->
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Advertisement Information System</span> - Data Order Customer</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Data Order Customer</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> Daftar</span> Order Customer
		<small class="display-block">Ini Merupakan Data Order Customer Yang Telah Dibuat</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Order Customer</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>Tanggal Pesan</th>
                            <th>Judul Iklan</th>
                            <th>Jenis Iklan</th>
                            <th>Biaya</th>
                            <th>Nama Pemesan</th>
                            <th>Status Order</th>
                            <th>Bukti Pembayaran</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($data as $key => $result)
                         <tr>
                        	<td align="left" style="width: 15%;">{!! $result->order_no !!}</td>
	                        <td align="left">{!! $result->judul_iklan !!}</td>
	                        <td align="left">{!! $result->medias->media_name !!}</td>
	                        <td align="left">{!! $result->total_biaya !!}</td>
	                        <td align="left"><i><b>{!! $result->users->name !!}</b></i></td>
	                      	<td>
	                      		@if($result->status_iklan == 'paid')
	                      			<label class="label label-success">PAID</label>
	                      		@elseif($result->status_iklan == 'pending')
	                      			<label class="label label-info">PENDING</label>
	                      		@elseif($result->status_iklan == 'overdue')
	                      			<label class="label label-warning">OVERDUE</label>
	                      		@else
	                      			<label class="label label-danger">CANCELED</label>
	                      		@endif
	                      	</td>
	                      	<td>{!! Helpers::tgl_indo($result->order_due) !!}</td>
	                        <td class="text-center">
	                           <div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
			                    		<li>
			                    			<a href="{{ url('/mvi-admin/order') }}/{!! base64_encode($result->order_id) !!}">
			                    				<i class="icon-eye"></i> Detail Order
			                    			</a>
			                    		</li>
			                    		<li>
			                    			<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_konfirm{{{ $result->order_id }}}">
			                    				<i class="icon-stamp"></i> Konfirmasi Order
			                    			</a>
			                    		</li>
										<li>
											<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_theme_danger{{{ $result->order_id }}}">
												<i class="icon-trash"></i> Cancel Order
											</a>
										</li>
									</ul>
								</div>
	                        </td>
                        </tr>

                        <!-- Danger modal -->
						<div id="modal_theme_danger{!! $result->order_id !!}" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-danger">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h6 class="modal-title">Cancel Order</h6>
									</div>

									<div class="modal-body">
										<h6 class="text-semibold">Apakah Anda Yakin Ingin Membatalkan Order No : <i> {!! $result->order_no !!} </i></h6>
									</div>

									<div class="modal-footer">
										{!! Form::open(['method' => 'DELETE', 'action' => ['OrderController@destroy', base64_encode($result->order_id)]]) !!}
			                            	{!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
			                            {!! Form::close() !!}
										<button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->

						<!-- Danger modal -->
						<div id="modal_konfirm{!! $result->order_id !!}" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-success">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h6 class="modal-title">Konfirmasi Order</h6>
									</div>

									<div class="modal-body">
										<h6 class="text-semibold">Apakah Anda Yakin Akan Mengkonfirmasi Order No : <i> {!! $result->order_no !!} </i></h6>
									</div>

									<div class="modal-footer">
										{!! Form::open(['method' => 'PATCH', 'action' => ['OrderController@update', base64_encode($result->order_id)]]) !!}
			                            	{!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
			                            {!! Form::close() !!}
										<button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/media/fancybox.min.js') !!}
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
{!! Html::script('admin_assets/js/plugins/tables/datatables/datatables.min.js') !!}
{!! Html::script('admin_assets/js/pages/gallery_library.js') !!}
@endsection