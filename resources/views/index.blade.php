<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	{!! SEO::generate(true) !!}
	<title>@yield('title')</title>
	<link rel="icon" href="">
	<!-- Global stylesheets -->
	{!! Minify::stylesheet('/admin_assets/css/icons/icomoon/styles.css') !!}
	{!! Minify::stylesheet('/admin_assets/css/minified/bootstrap.min.css') !!}
	{!! Minify::stylesheet('/admin_assets/css/minified/core.min.css') !!}
	{!! Minify::stylesheet('/admin_assets/css/minified/components.min.css') !!}
	{!! Minify::stylesheet('/admin_assets/css/minified/colors.min.css') !!}
	{!! Minify::stylesheet('/admin_assets/css/extras/animate.min.css') !!}
	{!! Minify::stylesheet('/admin_assets/css/custom.css') !!}
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

	@yield('style')
	<!-- /global stylesheets -->
	<style type="text/css">
	    .map-ku {
	        border-radius: 4px;
	        border:1px solid#ddd;
	        overflow: hidden;
	    }

	    .map-ku input {
	        margin-top: 10px;
	        width: 300px;
	        height: 34px;
	    }
	    input[type="date"]:hover::-webkit-calendar-picker-indicator {
		  color: red;
		}
		input[type="date"]:hover:after {
		  content: " Date Picker ";
		  color: #555;
		  padding-right: 5px;
		}
		input[type="date"]::-webkit-inner-spin-button {
		  /* display: none; <- Crashes Chrome on hover */
		  -webkit-appearance: none;
		  margin: 0;
		}
    </style>
	<!-- Core JS files -->

	<!-- /theme JS files -->

</head>

<body class="navbar-top">

	<!-- Main navbar -->
	<div class="navbar navbar-default navbar-fixed-top header-highlight">
		<div class="navbar-header" >
			<a class="navbar-brand" href="{{ url('/mvi-admin/home') }}" style="color:#fff;line-height:25px">
				<img src="{{ asset('images/website/logo/logo.png') }}">
			</a>
			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<p class="navbar-text"><span class="label bg-primary">Online</span></p>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ asset(Auth::user()->image) }}" alt="" class="img-circle img-sm">
						<span>{!! Auth::user()->name !!}</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ url('/mvi-admin/logout') }}"><i class="icon-switch2"></i> Keluar Sistem</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="{{ asset(Auth::user()->image) }}" class="img-circle img-sm" title="{!! Auth::user()->name !!}"></a>
								<div class="media-body">
									<span class="media-heading text-semibold">{!! Auth::user()->name !!}</span>
									<div class="text-size-mini text-muted">
										<i class="icon-key text-size-small"></i> &nbsp;{!! Auth::user()->email !!}
									</div>
								</div>

							</div>
						</div>
					</div>
					<!-- /user menu -->

					@if(Auth::user()->roles_id != 3)
						<!-- Main navigation -->
						<div class="sidebar-category sidebar-category-visible">
							<div class="category-content no-padding">
								<ul class="navigation navigation-main navigation-accordion">
									<li class="#">
										<a href="{{ url('/mvi-admin/home') }}"><i class="icon-home4"></i> <span>Dashboard</span></a>
									</li>
									<li class="#">
										<a href="{{ url('/mvi-admin/m/new-design') }}"><i class="icon-gallery"></i> <span>Buat Design Sendiri</span></a>
									</li>
									<li class="#">
										<a href="{{ url('/mvi-admin/pasang-iklan') }}"><i class="icon-cart-add"></i> <span>Pasang Iklan</span></a>
									</li>
									<li class="#">
										<a href="#"><i class="icon-cash3"></i> <span>Data Pembayaran</span></a>
										<ul>
											<li><a href="{{ url('/mvi-admin/m/pembayaran') }}" id="layout1">Upload Bukti </a></li>
											<li><a href="{{ url('/mvi-admin/m/history-pembayaran') }}" id="layout1">History Pembayaran </a></li>
										</ul>
									</li>
									<li>
										<a href="#"><i class="icon-copy"></i> <span>Data Order</span></a>
										<ul>
											<li><a href="{{ url('/mvi-admin/m/list-order') }}" id="layout1">List Order </a></li>
											<li><a href="{{ url('/mvi-admin/m/history-order') }}" id="layout1">History Order </a></li>
										</ul>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/faq') }}"><i class="icon-lifebuoy"></i> <span>Customer Service (FAQ)</span></a>
									</li>
									<li class="navigation-header"><span>Pengaturan</span> <i class="icon-menu" title="Pengaturan Website"></i></li>
									<li class="#">
										<a href="{{ url('/mvi-admin/auth') }}"><i class="icon-cog"></i> <span>Pengaturan User</span></a>
									</li>
									<li class="#">
										<a href="{{ url('/mvi-admin/logout') }}"><i class="icon-exit"></i> <span>Logging Out</span></a>
									</li>
								</ul>
							</div>
						</div>
					@else
						<!-- Main navigation -->
						<div class="sidebar-category sidebar-category-visible">
							<div class="category-content no-padding">
								<ul class="navigation navigation-main navigation-accordion">
									<li class="#">
										<a href="{{ url('/mvi-admin/home') }}"><i class="icon-home4"></i> <span>Dashboard</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/customer') }}"><i class="icon-users"></i> <span>Data Customer</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/media') }}"><i class="icon-image3"></i> <span>Data Media</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/jenis-iklan') }}"><i class="icon-drawer3"></i> <span>Data Jenis Iklan</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/kategori') }}"><i class="icon-stack"></i> <span>Data Kategori</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/ukuran') }}"><i class="icon-angle"></i> <span>Data Ukuran</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/paket') }}"><i class="icon-car"></i> <span>Data Paket</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/metode-bayar') }}"><i class="icon-library2"></i> <span>Data Metode Pembayaran</span></a>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/template') }}"><i class="icon-gallery"></i> <span>Data Template</span></a>
									</li>
									<li class="#">
										<a href="{{ url('/mvi-admin/pembayaran') }}"><i class="icon-cash3"></i> <span>Data Pembayaran</span></a>
									</li>
									<li>
										<a href="#"><i class="icon-copy"></i> <span>Data Order</span></a>
										<ul>
											<li><a href="{{ url('/mvi-admin/order') }}" id="layout1">List Order </a></li>
											<li><a href="{{ url('/mvi-admin/pending-order') }}" id="layout1">Pending Order </a></li>
										</ul>
									</li>
									<li>
										<a href="{{ url('/mvi-admin/faq') }}"><i class="icon-lifebuoy"></i> <span>Customer Service (FAQ)</span></a>
									</li>
									<li class="navigation-header"><span>Pengaturan</span> <i class="icon-menu" title="Pengaturan Website"></i></li>
									<li class="#">
										<a href="{{ url('/mvi-admin/auth') }}"><i class="icon-cog"></i> <span>Pengaturan User</span></a>
									</li>
									<li class="#">
										<a href="{{ url('/mvi-admin/logout') }}"><i class="icon-exit"></i> <span>Logging Out</span></a>
									</li>
								</ul>
							</div>
						</div>
					@endif

				</div>
			</div>
			<!-- /main sidebar -->
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				@yield('content')
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- FOOTER JS -->
	{!! Html::script('admin_assets/js/plugins/loaders/pace.min.js') !!}
	{!! Html::script('admin_assets/js/core/libraries/jquery.min.js') !!}
	{!! Html::script('admin_assets/js/core/libraries/bootstrap.min.js') !!}
	{!! Html::script('admin_assets/js/plugins/loaders/blockui.min.js') !!}
	<!-- /core JS files -->

	<!-- Theme JS files -->
	{!! Html::script('admin_assets/js/plugins/visualization/d3/d3.min.js') !!}
	{!! Html::script('admin_assets/js/plugins/visualization/d3/d3_tooltip.js') !!}
	{!! Html::script('admin_assets/js/plugins/forms/styling/switchery.min.js') !!}
	{!! Html::script('admin_assets/js/plugins/forms/styling/uniform.min.js') !!}
	{!! Html::script('admin_assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}
	{!! Html::script('admin_assets/js/plugins/ui/moment/moment.min.js') !!}
	{!! Html::script('admin_assets/js/pages/extension_blockui.js') !!}
	{!! Html::script('admin_assets/js/pages/components_popups.js') !!}
	{!! Html::script('admin_assets/js/core/app.js') !!}
	{!! Html::script('admin_assets/js/plugins/notifications/sweet_alert.min.js') !!}
	{!! Html::script('js/jquery.email-autocomplete.js') !!}
	@include('sweet::alert')
	
	@yield('script')
	{!! Html::script('admin_assets/js/uprak.js') !!}

	<script type="text/javascript">

	 function deleteIt(that){
		swal({   
			title: "Apa Anda Yakin ?",   
			text: "Anda Akan Menghapus Data Ini",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Ya, Hapus Data!",   
			closeOnConfirm: false 
		}, function(){   
			swal({   
					title: "Deleted",   
					text: "Data Anda Telah Di Hapus",   
					type: "success"
			},function() {
				redirect($(that).attr('data-url'));
			}); 
			
		});	
	}

	function showTab(that){
		var element = 'a[href="'+$(that).attr('element-id')+'"]';
		$(element).tab('show');
	}	
	</script>

	<script type="text/javascript">
        var _validFileExtensions = [".jpg", ".jpeg", ".png"];    
        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                 if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                     
                    if (!blnValid) {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
    </script>
	
</body>
</html>