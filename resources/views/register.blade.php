<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Advertisement Information System</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin_assets/css/minified/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin_assets/css/minified/core.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin_assets/css/minified/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin_assets/css/minified/colors.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin_assets/css/extras/animate.min.css') }}" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	<!-- /global stylesheets -->
</head>

<body>

	<!-- Page container -->
	<div class="page-container login-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
      		<div class="content-wrapper" style="background-image: url({{ asset('images/bg.jpg') }}); background-size: cover;">

				<!-- Content area -->
				<div class="content">

					<!-- Registration form -->
					{!! Form::open(['method' => 'POST', 'id' => 'form-register']) !!}
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
											<h5 class="content-group-lg">Buat Akun Baru <small class="display-block">Harap Mengisi Form Dengan Lengkap</small></h5>
										</div>

										<div class="form-group has-feedback">
											<select data-placeholder="Pilih Hak Akses" class="select" name="role" id="tipe">
													<option selected="selected">Pilih Tipe Member</option>
													<option value="1">Perseorangan</option>
													<option value="2">Perusahaan</option>
											</select>
										</div>

										<div class="form-group has-feedback">
											<input type="text" class="form-control" placeholder="Masukkan Username" name="username">
											<div class="form-control-feedback">
												<i class="icon-user-plus text-muted"></i>
											</div>
										</div>

										<div class="form-group has-feedback" id="npwp">
											<input type="text" class="form-control" placeholder="Masukkan No. NPWP" name="npwp">
											<div class="form-control-feedback">
												<i class="icon-lock text-muted"></i>
											</div>
										</div>

										<div class="form-group has-feedback" id="cp">
											<input type="text" class="form-control" placeholder="Masukkan Penanggung Jawab" name="cp">
											<div class="form-control-feedback">
												<i class="icon-cog text-muted"></i>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="text" class="form-control" placeholder="Nama Lengkap" name="name">
													<div class="form-control-feedback">
														<i class="icon-user-check text-muted"></i>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="text" class="form-control" placeholder="No Handphone" name="contact">
													<div class="form-control-feedback">
														<i class="icon-phone text-muted"></i>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="text" class="form-control" placeholder="Alamat Lengkap" name="address">
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="text" class="form-control" placeholder="Alamat Lain Yang Bisa Dikunjungi" name="address_2">
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="password" class="form-control" placeholder="Create password" name="password">
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="password" class="form-control" placeholder="Repeat password" name="r_password">
													<div class="form-control-feedback">
														<i class="icon-user-lock text-muted"></i>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="email" class="form-control" placeholder="Your email" name="email">
													<div class="form-control-feedback">
														<i class="icon-mention text-muted"></i>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">
													<input type="email" class="form-control" placeholder="Repeat email" name="r_email">
													<div class="form-control-feedback">
														<i class="icon-mention text-muted"></i>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group">
										<center>
											<p> {!! captcha_img() !!} </p>
											<input type="text" name="captcha" class="form-control" placeholder="Masukkan Captcha Diatas">
										</center>
										</div>

										<div class="form-group">

											<div class="checkbox">
												<label>
													<input type="checkbox" class="styled">
													Accept <a href="#">terms of service</a>
												</label>
											</div>

										</div>

										<div class="text-right">
											<a href="{{ url('/') }}" class="btn btn-link login"><i class="icon-arrow-left13 position-left"></i> Sudah Punya Akun ? Login</a>
											<button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Submit</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<!-- /registration form -->

					<!-- Footer -->
			        <div class="footer text-muted">
			          <p style="color: #000;">&copy; 2017&nbsp;<a href="https://www.mvi.web.id" style="color: #000;" target="_blank"> PT Media Virtual Indonesia - All Rights Reserved</a></p>
			        </div>
			        <!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('admin_assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/plugins/notifications/noty.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/plugins/ui/prism.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('admin_assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/pages/components_notifications_other.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin_assets/js/pages/extension_blockui.js') }}"></script>
	<!-- /theme JS files -->

	<script type="text/javascript" src="{{ asset('admin_assets/js/uprak.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/auth.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
	{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
	{!! Html::script('admin_assets/js/pages/form_select2.js') !!}
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	{!! Html::script('js/sweetalert.min.js') !!}
	@include('sweet::alert')

	<script type="text/javascript">
		$('#npwp').hide();
		$('#cp').hide();
		$('#tipe').change(function () {
			if ($('#tipe').val() == "2") {
				$('#npwp').show();
				$('#cp').show();
			} else {
				$('#npwp').hide();
				$('#cp').hide();
			}
		});
	</script>
</body>
</html>
