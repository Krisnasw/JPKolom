@extends('index')

@section('title')
Advertisement Information System - Daftar Paket
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/paket') }}"><i class="icon-stack2 position-left"></i> Paket</a></li>
			<li class="active">Data Paket</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> Daftar</span> Paket
		<small class="display-block">List Daftar Paket</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Daftar Paket</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;">
					<a href="{{ url('/mvi-admin/paket/create') }}" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-plus3"></i></b> Tambah Paket Baru</a>
				</div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>No</th>
                            <th>Nama Paket</th>
                            <th>Kategori</th>
                            <th>Ukuran</th>
                            <th>Warna Paket</th>
                            <th>Jumlah Tayang</th>
                            <th>Harga Paket</th>
                            <th>Area Paket</th>
                            <th>Off Day</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@php $no = 0; @endphp
                    	@foreach($data as $key => $result)
                         <tr>
                        	<td align="center">{!! ++$no !!}</td>
	                        <td style="width:300px;">{!! Helpers::read_more($result->paket_name) !!}</td>
	                        <td align="left">{!! Helpers::read_more($result->category->kategori_name) !!}</td>
	                        <td align="left">{!! $result->size->ukuran_kolom !!} X {!! $result->size->ukuran_mm !!}</td>
	                        <td align="left">{!! $result->paket_color !!}</td>
	                        <td align="left">{!! $result->paket_jml_tayang !!}</td>
	                        <td align="left">Rp.{!! number_format($result->paket_price) !!}</td>
	                        <td align="left">{!! $result->paket_area !!}</td>
	                        <td align="left">{!! $result->paket_disabled_days !!}</td>
	                        <td class="text-center">
	                           <div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a href="{{ url('/mvi-admin/paket') }}/{!! base64_encode($result->paket_id) !!}/edit">
												<i class="fa fa-edit"></i> Ubah Paket
											</a>
										</li>
										<li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_theme_danger{{{ base64_decode($result->paket_id) }}}">
												<i class="fa fa-trash"></i> Hapus Paket
											</a>
										</li>
									</ul>
								</div>
	                        </td>
                        </tr>

                        <!-- Danger modal -->
						<div id="modal_theme_danger{{{ base64_decode($result->paket_id) }}}" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-danger">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h6 class="modal-title">Hapus Data</h6>
									</div>

									<div class="modal-body">
										<h6 class="text-semibold">Apakah Anda Yaking Menghapus <i> {!! $result->paket_name !!} </i></h6>
										<p>NB : Data Tidak Dapat Kembali Setelah Anda Hapus !</p>
									</div>

									<div class="modal-footer">
										{!! Form::open(array('method' => 'DELETE', 'route' => array('paket.destroy', base64_encode($result->paket_id)))) !!}
			                            	{!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
			                            {!! Form::close() !!}
										<button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/media/fancybox.min.js') !!}
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
{!! Html::script('admin_assets/js/plugins/tables/datatables/datatables.min.js') !!}
{!! Html::script('admin_assets/js/pages/gallery_library.js') !!}
@endsection