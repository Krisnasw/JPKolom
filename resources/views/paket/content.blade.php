@extends('index')

@section('title')
{{ ($type == "create") ? 'Buat Paket Baru' : 'Edit Paket' }} - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/paket') }}"><i class="icon-stack2 position-left"></i> Paket</a></li>
			<li class="active">Data Paket</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type == "create") ? 'Buat' : 'Update' }}</span> Paket
		<small class="display-block">
			{{ ($type == "create") ? 'Buat Paket Baru' : 'Perbarui Paket' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Paket</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
				@if($type == "create")
					{!! Form::open(['id' => 'form-paket', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'PaketController@store', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Kategori <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Iklan" class="select" name="kategori_id">
										<option value="selected">Pilih Jenis Kategori</option>
										@foreach($kat as $ik => $key)
											<option value="{!! $key->kategori_id !!}">{!! $key->kategori_name !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Ukuran <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Iklan" class="select" name="ukuran_id">
										<option value="selected">Pilih Jenis Ukuran</option>
										@foreach($uk as $ik => $key)
											<option value="{!! $key->ukuran_id !!}">{!! $key->ukuran_kolom !!} X {!! $key->ukuran_mm !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Paket" name="paket_name" 
								value="{{ ($type=='create') ? '' : $data->paket_name }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Warna Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Warna Iklan" name="paket_color" 
								value="{{ ($type=='create') ? '' : $data->paket_color }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jumlah Tayang <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="number" placeholder="0 - 100" name="paket_jml_tayang" 
								value="{{ ($type=='create') ? '' : $data->paket_jml_tayang }}" required min="0">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Harga Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="number" placeholder="Masukkan Harga Paket" name="paket_price" 
								value="{{ ($type=='create') ? '' : $data->paket_price }}" required min="0">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Area Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Area Iklan" name="paket_area" 
								value="{{ ($type=='create') ? '' : $data->paket_area }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jumlah Huruf <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="number" placeholder="0-500" name="paket_jml_huruf" 
								value="{{ ($type=='create') ? '' : $data->paket_jml_huruf }}" required min="0">
							</div>
						</div>

						<!-- Basic example -->
						<div class="form-group">
							<label class="col-lg-2 control-label">Hari Tidak Aktif <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control tokenfield" value="{{ ($type=='create') ? '' : $data->paket_disabled_days }}" name="paket_disabled_days[]">
							</div>
						</div>
						<!-- /basic example -->

						<!-- Basic example -->
						<div class="form-group">
							<label class="col-lg-2 control-label">Hari Aktif <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control tokenfield" value="{{ ($type=='create') ? '' : $data->paket_active_days }}" name="paket_active_days[]">
							</div>
						</div>
						<!-- /basic example -->

						<div class="form-group">
							<label class="col-lg-2 control-label">Deskripsi Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<textarea class="summernote" name="paket_description">{{ ($type=='create') ? 'Masukkan Deskripsi Paket' : $data->paket_description }}</textarea>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Paket' : 'Ubah Paket' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@else
					{!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['PaketController@update', base64_encode($data->paket_id)], 'files' => true, 'id' => 'form-paket']) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Kategori <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Iklan" class="select" name="kategori_id">
										<option value="selected">Pilih Jenis Kategori</option>
										@foreach($kat as $ik => $key)
											<option value="{!! $key->kategori_id !!}">{!! $key->kategori_name !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Ukuran <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Iklan" class="select" name="ukuran_id">
										<option value="selected">Pilih Jenis Ukuran</option>
										@foreach($uk as $ik => $key)
											<option value="{!! $key->ukuran_id !!}">{!! $key->ukuran_kolom !!} X {!! $key->ukuran_mm !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Paket" name="paket_name" 
								value="{{ ($type=='create') ? '' : $data->paket_name }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Warna Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Warna Iklan" name="paket_color" 
								value="{{ ($type=='create') ? '' : $data->paket_color }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jumlah Tayang <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="number" placeholder="0 - 100" name="paket_jml_tayang" 
								value="{{ ($type=='create') ? '' : $data->paket_jml_tayang }}" required min="0">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Harga Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="number" placeholder="Masukkan Harga Paket" name="paket_price" 
								value="{{ ($type=='create') ? '' : $data->paket_price }}" required min="0">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Area Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Area Iklan" name="paket_area" 
								value="{{ ($type=='create') ? '' : $data->paket_area }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jumlah Huruf <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="number" placeholder="0-500" name="paket_jml_huruf" 
								value="{{ ($type=='create') ? '' : $data->paket_jml_huruf }}" required min="0">
							</div>
						</div>

						<!-- Basic example -->
						<div class="form-group">
							<label class="col-lg-2 control-label">Hari Tidak Aktif <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control tokenfield" value="{{ ($type=='create') ? '' : $data->paket_disabled_days }}" name="paket_disabled_days">
							</div>
						</div>
						<!-- /basic example -->

						<!-- Basic example -->
						<div class="form-group">
							<label class="col-lg-2 control-label">Hari Aktif <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="text" class="form-control tokenfield" value="{{ ($type=='create') ? '' : $data->paket_active_days }}" name="paket_active_days">
							</div>
						</div>
						<!-- /basic example -->

						<div class="form-group">
							<label class="col-lg-2 control-label">Deskripsi Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<textarea class="summernote" name="paket_description">{{ ($type=='create') ? 'Masukkan Deskripsi Paket' : $data->paket_description }}</textarea>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Paket' : 'Ubah Paket' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@endif
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script>
  (function($){
    $(function() {
      $("#email").emailautocomplete({
        domains: ["example.com"] //add your own domains
      });
    });
  }(jQuery));
</script>

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ ($type=='create') ? Helpers::img_holder() : asset($data->image) }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-paket").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-paket")[0] );

			$.ajax({
				url: 		$("#form-paket").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-paket'),'Please Wait , {{ ($type =="create") ? "Menambahkan Paket" : "Memperbarui Paket" }}','#fff');
				}
			})
			.done(function(response) {
				$('#form-paket').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : '{{ ($type =="create") ? "Paket Berhasil Dibuat" : "Paket Telah Diperbaharui" }}'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/paket') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-paket').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

{!! Html::script('admin_assets/js/plugins/editors/summernote/summernote.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/uniform.min.js') !!}
{!! Html::script('admin_assets/js/pages/editor_summernote.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/tags/tagsinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/tags/tokenfield.min.js') !!}
{!! Html::script('admin_assets/js/plugins/ui/prism.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_tags_input.js') !!}
{!! Html::script('admin_assets/js/pages/form_select2.js') !!}
@endsection