@extends('index')

@section('title')

Support FAQ's - Advertisement Information System

@endsection

@section('content')

<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Support - FAQ's</span> - Advertisement Information System</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/home') }}"><i class="icon-home2 position-left"></i> Support - FAQ's</a></li>
			<li class="active">Advertisement Information System</li>
		</ul>
	</div>
</div>

<!-- Content area -->
<div class="content">


	<!-- Questions area -->
	<h4 class="text-center content-group">
		Customer Service - Advertisement Information System
		<small class="display-block">Posting Pertanyaan Anda, Dan Tunggu Jawabannya</small>
	</h4>

		<div class="row">
			<div class="col-lg-9">

				<!-- Questions list -->
				<div class="panel-group panel-group-control panel-group-control-right">
				@foreach($data as $datas => $key)
					<div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title">
								<a class="collapsed" data-toggle="collapse" href="#question1{{{ $key->faq_id }}}">
									<i class="icon-help position-left text-slate"></i> {!! $key->faq_title !!} -  Oleh {!! $key->users->name !!}
								</a>
							</h6>
						</div>

						<div id="question1{{{ $key->faq_id }}}" class="panel-collapse collapse">
							<div class="panel-body">
								{!! $key->faq_description !!}
							</div>

							<div class="panel-footer panel-footer-transparent">
								<ul>
									<li class="text-muted">Latest update: {!! Helpers::tgl_indo($key->created_at) !!}</li>
								</ul>

								<ul class="pull-right">
									<li>
										<a href="#" data-toggle="modal" data-target="#modal_comment{{{ $key->faq_id }}}">
											<i class="icon-pencil position-left"></i>
											Komentar
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<!-- Warning modal -->
					<div id="modal_comment{{{ $key->faq_id }}}" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-success">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h6 class="modal-title">Mari Bantu Jawab</h6>
								</div>

								{!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'form-komen', 'action' => 'AuthController@postKomen']) !!}
									<div class="modal-body">
									<!-- List Komentar -->
									@if($komen->count() < 1)
										<p> Tidak Ada Jawaban Untuk Pertanyaan Ini </p>
									@else
										@foreach($komen as $res => $data)
											@if($data->faq_id == $key->faq_id)
												<div class="media-left media-middle">
													<a href="#">
														<img src="{{ asset($data->users->image) }}" class="img-circle" alt="">
													</a>
												</div>

												<div class="media-body">
													<div class="media-heading text-semibold">{!! $data->users->name !!}</div>
													<span class="text-muted">{!! $data->users->email !!}</span>

													<p>
														{!! $data->komen_isi !!}
													</p>
												</div>
												<hr />
											@endif
										@endforeach
									@endif
									<!-- List Komentar /-->
										<br />

										<div class="form-group">
											<div class="col-sm-12">
												<input type="hidden" name="id_faq" value="{!! $key->faq_id !!}">
												<textarea class="form-control" name="komen" placeholder="Masukkan Komentar Disini">
												</textarea>
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-success">Submit form</button>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
					<!-- /warning modal -->
				@endforeach
				</div>

			</div>

			<div class="col-lg-3">

				<!-- Search -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title">Cari Pertanyaan</h6>
						<div class="heading-elements">
							<ul class="icons-list">
					            <li><a href="#"><i class="icon-stats-bars"></i></a></li>
					        </ul>
				        </div>
					</div>

					<div class="panel-body">
							{!! Form::open(['method' => 'GET']) !!}
							<div class="input-group content-group">
									<input type="text" class="form-control input-lg" placeholder="Search our help center">

								<div class="input-group-btn">
									<button type="submit" class="btn btn-primary btn-lg btn-icon"><i class="icon-search4"></i></button>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /search -->

				<!-- Navigation -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title">Navigasi</h6>
						<div class="heading-elements">
							<ul class="icons-list">
					           <li>
					           	<a href="#">
					           		<i class="icon-cog3"></i>
					           	</a>
					           </li>
					    	</ul>
				        </div>
					</div>

					<div class="panel-body">
						<a href="#" data-toggle="modal" data-target="#modal_new_faq" class="btn bg-teal btn-block">Buat Pertanyaan <i class="icon-comment position-right"></i></a>
					</div>

					<div class="list-group list-group-borderless mb-5">
						<a href="#" data-toggle="modal" data-target="#modal_theme_warning" class="list-group-item"><i class="icon-mail5"></i> Contact us</a>
					</div>
				</div>
				<!-- /navigation -->

				<!-- Warning modal -->
					<div id="modal_new_faq" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-info">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h6 class="modal-title">Form Buat Pertanyaan</h6>
								</div>

								{!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'form-ask', 'action' => 'AuthController@postFaq']) !!}
									<div class="modal-body">
										<div class="form-group">
											<label class="control-label col-sm-3">Judul Pertanyaan</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Masukkan Judul Pertanyaan" class="form-control" name="judul">
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-3">Isi Pertanyaan</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Masukkan Isi Pertanyaan" class="form-control" name="isi">
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-3">Tag Pertanyaan</label>
											<div class="col-sm-9">
												<input type="text" class="form-control tokenfield" name="tag">
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Submit form</button>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				<!-- /warning modal -->

				<!-- Warning modal -->
					<div id="modal_theme_warning" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-warning">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h6 class="modal-title">Kontak Kami</h6>
								</div>

								<div class="modal-body">
									<h6 class="text-semibold">Hallo, {!! Auth::user()->name !!}</h6>
									<p>Jika Anda Mendapat Kendala, Kebingungan, Atau Terjadi Kesalahan Silahkan Anda Datang Ke Kantor Kami Bertempat Di Bawah Ini</p>

									<hr>

									<h6 class="text-semibold">Lokasi Kami :</h6>
									<p>No. Telepon : (031) 8287999 </p>
									<p>Alamat : Jalan Jenderal A. Yani No. 88 Ketintang Gayungan</p>
									<p>Kota : Surabaya </p>
									<p>Negara : Indonesia </p>
									<p>Buka : Senin - Jumat (09:00 - 17:00) / Sabtu (09:00 - 13:00)</p>
									<p>Situs Web : <a href="https://www.jawapos.com">www.jawapos.com</a></p>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-warning">Save changes</button>
								</div>
							</div>
						</div>
					</div>
				<!-- /warning modal -->


		</div>
	</div>
	<!-- /questions area -->

</div>
<!-- /content area -->

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/forms/tags/tagsinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/tags/tokenfield.min.js') !!}
{!! Html::script('admin_assets/js/plugins/ui/prism.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_tags_input.js') !!}
<script type="text/javascript">
	$('#form-ask').submit(function (e) {
		e.preventDefault();
		var formData = new FormData($("#form-ask")[0]);

		$.ajax({
				url: 		$("#form-ask").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-ask'),'Please Wait , Menambahkan Pertanyaan','#fff');
				}
			})
			.done(function(response) {
				$('#form-ask').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : 'Pertanyaan Berhasil Dibuat'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/faq') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-ask').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
	})
</script>
@endsection