@extends('index')

@section('title')
Akun Administrator
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/auth') }}"><i class="icon-stack2 position-left"></i> Akun User Advertisement Information System</a></li>
			<li class="active">Akun Advertisement Information System</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> Akun User</span> Advertisement Information System
		<small class="display-block">Mengatur Akun User Pada Website</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="tabbable tab-content-bordered">
				<ul class="nav nav-tabs nav-tabs-highlight">
					<li class="active">
						<a href="#tab-identity"  data-toggle="tab" aria-expanded="true"><i class="fa fa-globe position-left"></i> Akun Administrator</a>
					</li>
				</ul>

				<div class="tab-content ">
					<div class="tab-pane active" id="tab-identity">
						<div  style="padding:20px;">
							{!! Form::open(['id' => 'form-sosmed', 'class' => 'form-horizontal', 'action' => 'AuthController@updateUserLogin', 'files' => true, 'method' => 'POST']) !!}
								<div class="form-group">
									<label class="col-lg-2 control-label">Name <span class="text-danger"><b>*</b></span></label>
									<div class="col-lg-10">
										<input class="form-control" type="text" name="fullname" value="{!! Auth::user()->name !!}" placeholder="Nama Akun Anda" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Email <span class="text-danger"><b>*</b></span></label>
									<div class="col-lg-10">
										<input class="form-control" type="email" name="email" value="{!! Auth::user()->email !!}" placeholder="Email Akun Anda" required readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Username <span class="text-danger"><b>*</b></span></label>
									<div class="col-lg-10">
										<input class="form-control" type="text" name="username" value="{!! Auth::user()->username !!}" placeholder="Nama Pengguna Akun Anda" required readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Alamat Lengkap <span class="text-danger"><b>*</b></span></label>
									<div class="col-lg-10">
										<input class="form-control" type="text" name="address" value="{!! Auth::user()->address !!}" placeholder="Alamat Lengkap Anda" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">No. Handphone <span class="text-danger"><b>*</b></span></label>
									<div class="col-lg-10">
										<input class="form-control" type="text" name="contact" value="{!! Auth::user()->contact !!}" placeholder="No. Telp Anda" required>
									</div>
								</div>

								<div class="form-group">
										<label class="col-lg-2 control-label">Foto Profil User</label>
										<div class="col-lg-10">
											<div class="media no-margin-top">
												<div class="media-left">
													<img src="{{ asset(Auth::user()->image) }}" style="width:auto; height: 200px;" class="img-rounded" alt="">
												</div>
												<br>
												<div class="media-body">
													<input id="koko" type="file" class="file-styled" name="image" onchange="profilUpload(this); ValidateSingleInput(this);">
													<span class="help-block"> *Jangan Di Ubah Jika Tidak Ada Perubahan, File Maks 2MB</span>
													<span class="help-block"> *Jika Gambar Tidak Bisa Diupload, Gambar Terlalu Kecil</span>
												</div>
											</div>
										</div>
									</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Ganti Password </label>
									<div class="col-lg-10">
										<div id="not-change">
											<button type="button" class="btn bg-orange" onclick="changePass(true)"><i class="icon-key"></i> Ganti Password</button>
											<div style="height:20px;"></div>
										</div>
										<div id="change" style="display:none">
											<div class="input-group">
												<input class="form-control" type="password" name="passwordold" class="form-control" placeholder="Password lama anda">
												<span class="input-group-addon bg-pink" onclick="showPass(this)"><i class="icon-eye"></i></span>
											</div>
											<div style="height:20px;"></div>
											<div class="input-group">
												<input class="form-control" type="password" name="password" class="form-control" placeholder="Password baru anda">
												<span class="input-group-addon bg-pink" onclick="showPass(this)"><i class="icon-eye"></i></span>
											</div>
											<div style="height:20px;"></div>
											<div class="input-group">
												<input class="form-control" type="password" name="passwordconf" class="form-control" placeholder="Konfirmasi password baru anda">
												<span class="input-group-addon bg-pink" onclick="showPass(this)"><i class="icon-eye"></i></span>
											</div>
											<div style="height:20px;"></div>
											<button type="button" class="btn btn-success" onclick="changePass(false)"><i class="fa fa-times"></i> Batal Ganti password</button>
											
										</div>
										
									</div>
								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Save Configuration <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							{!! Form::close() !!}
						</div>
						<div class="clearfix"></div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
<script type="text/javascript">

	function profilUpload(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	        	$(input).parent().parent().prev().find('a img').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	} 
	
 	function showPass(that) {
 		var type = $(that).prev().attr('type');
 		if(type=="password"){
 			$(that).prev().attr('type','text');
 		}
 		else {
 			$(that).prev().attr('type','password');
 		}
 	}

 	function changePass(type) {
 		if(type==true){
 			$("#not-change").fadeOut(function(){
 				$("#change").find("input").prop("required",true);
 				$("#change").fadeIn();
 			})
 		}
 		else {
 			$("#change").fadeOut(function(){
 				$("#change").find("input").prop("required",false);
 				$("#not-change").fadeIn();
 			})
 		}
 	}

	$("#form-sosmed").submit(function(e) {
			e.preventDefault();
			var formData = new FormData( $("#form-sosmed")[0] );
			$.ajax({
				url: 		$("#form-sosmed").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-sosmed'),'Tunggu , Sedang Menyimpan Akun Administrator','#fff');		
				}
			})
			.done(function(response) {

				$('#form-sosmed').unblock();
				sweetAlert({
					title: 	((response.status==false) ? "Opps!" : "Akun Administrator Di Simpan"),
					text: 	response.msg,
					type: 	((response.status==false) ? "error" : "success"),
				},
				function() {
					if(response.status!=false){
						redirect("{{ url('/mvi-admin/auth') }}");		
						return;
					}
					redirect("{{ url('/mvi-admin/auth') }}");
				});

			})
			.fail(function() {
			    $('#form-sosmed').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Kembali",
					type: 	"error",
				},
				function(){
					redirect("{{ url('/mvi-admin/auth') }}");
				});
			 })
			
		})

	
</script>
@endsection