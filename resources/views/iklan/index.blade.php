@extends('index')

@section('title')
Pasang Iklan Baru - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/pasang-iklan') }}"><i class="icon-stack2 position-left"></i> Pasang Iklan</a></li>
			<li class="active">Pasang Iklan Baru</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> Pasang Iklan</span> Advertisement Information System
		<small class="display-block">
			Pasang Iklan Baru
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Pasang Iklan</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
					{!! Form::open(['id' => 'form-media', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'AuthController@postAdv', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">No. Order <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" name="no_order" value="{!! $nomer !!}" required readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Pilih Media <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Media" class="select" name="media_id" id="m_media">
									<option value="0" selected="selected">Pilih Jenis Media</option>
									@foreach($media as $ik => $key)
										<option value="{!! $key->media_id !!}">{!! $key->media_name !!}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Iklan" class="select" name="iklan_id" id="i_iklan">
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Kategori <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Kategori" class="select" name="kategori_id" id="k_kategori">
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Jenis Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Paket" class="select" name="paket_id" id="p_paket">
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Tanggal Muat <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-5">
								<div class="input-group" id="tgl_awal">
									<span class="input-group-addon"><i class="icon-calendar5"></i></span>
									<input type="date" class="form-control pickadate-year" placeholder="Try me&hellip;" name="tgl_awal">
								</div>
							</div>
							<div class="col-lg-5">
								<div class="input-group" id="tgl_akhir">
									<span class="input-group-addon"><i class="icon-calendar5"></i></span>
									<input type="date" class="form-control pickadate-year" placeholder="Try me&hellip;" name="tgl_akhir">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Harga Paket </label>
							<div class="col-lg-10">
								<div id="hrg_paket"></div>
								<div id="rego_paket"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Total Biaya ( Harga Paket + PPN 10% ) </label>
							<div class="col-lg-10">
								<div id="tot_biaya"></div>
								<div id="ppn_id"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Metode Pembayaran <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Metode Pembayaran" class="select" name="metode_id">
									<option selected disabled>Pilih Jenis Metode Pembayaran</option>
									@foreach($metode as $res => $key)
										<option value="{!! $key->metode_id !!}">{!! $key->metode_bank !!}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Judul Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Judul Iklan" name="judul_iklan" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Deskripsi Iklan<span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<textarea class="summernote" name="paket_description">Masukkan Deskripsi Iklan</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Desain Iklan<span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="file" class="file-input-custom" name="image" accept="image/*">
									<span class="help-block">  
									NB : Ukuran Maksimum 2MB , Hanya File Gambar ( Jangan Ubah Jika Tidak Ada Perubahan )
									</span>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
							<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
						</div>
					{!! Form::close() !!}
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

{!! Html::script('admin_assets/js/plugins/editors/summernote/summernote.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/uniform.min.js') !!}
{!! Html::script('admin_assets/js/pages/editor_summernote.js') !!}

<script>
    $(document).ready(function () {

	$('#tgl_awal').hide();
	$('#tgl_akhir').hide();
	
		$('#m_media').on('change', function () {
			// console.log("its change bosq");

			var media_id = $(this).val();
			var div = $(this).parent();
			var hasil = "";

	  		$.ajax({
	  			type: 'get',
	  			url : '{{ url('/mvi-admin') }}/api/ajax-jenis?media_id',
	  			data : { 'media_id' : media_id },
	  			success:function (data) {
	  				hasil+='<option value="0" selected disabled> Pilih Jenis Iklan </option>';
	  				for (var i = 0; i < data.length; i++) {
	  					hasil+='<option value="'+data[i].iklan_id+'">'+data[i].iklan_type+'</option>';
	  				}

	  				$('#i_iklan').append(hasil);

	  			},
	  			error:function () {
	  				sweetAlert({
						title: 	"Opss!",
						text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
						type: 	"error",
					},
					function() {
						redirect("{{ url('/') }}");
						return;
					});
	  			}
	  		});

		});

		$('#i_iklan').on('change', function() {

			var iklan_id = $(this).val();
			var div = $(this).parent();
			var hasil = "";

			$.ajax({
				type: 'get',
	  			url : '{{ url('/mvi-admin') }}/api/ajax-kategori?iklan_id',
	  			data : { 'iklan_id' : iklan_id },
	  			success:function (data) {
	  				hasil+='<option value="0" selected disabled> Pilih Jenis Kategori </option>';
	  				for (var i = 0; i < data.length; i++) {
	  					hasil+='<option value="'+data[i].kategori_id+'">'+data[i].kategori_name+'</option>';
	  				}

	  				$('#k_kategori').append(hasil);

	  			},
	  			error:function () {
	  				sweetAlert({
						title: 	"Opss!",
						text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
						type: 	"error",
					},
					function() {
						redirect("{{ url('/') }}");
						return;
					});
	  			}
			});

		});

		$('#k_kategori').on('change', function () {

			var kategori_id = $(this).val();
			var hasil = "";
			var rego = "";
			var biaya = "";
			var pct = "";
			var tayang = "";
			var ppn = "";
			var hrg_paket = "";

			$.ajax({
				type: 'get',
	  			url : '{{ url('/mvi-admin') }}/api/ajax-paket?kategori_id',
	  			data : { 'kategori_id' : kategori_id },
	  			success:function (data) {
	  				hasil+='<option value="0" selected disabled> Pilih Jenis Paket </option>';
	  				for (var i = 0; i < data.length; i++) {
	  					hasil+='<option value="'+data[i].paket_id+'">'+data[i].paket_name+'</option>';
	  					rego+='<span class="text-semibold" id="hrg_paket">Rp.'+data[i].paket_price+'</span>';
	  					pct = data[i].paket_price + data[i].paket_price * 10 / 100;
	  					biaya+='<input type="text" class="form-control" id="tot_biaya" name="total_biaya" value="'+pct+'" readonly>';
	  					tayang+= data[i].paket_jml_tayang;
	  					ppn+= '<input type="hidden" class="form-control" id="ppn_id" name="ppn" value="'+data[i].paket_price * 10 / 100+'">';
	  					hrg_paket+= '<input type="hidden" class="form-control" id="hrg_paket" name="regone" value="'+data[i].paket_price+'">';
	  				}

	  				$('#p_paket').append(hasil);
	  				$('#hrg_paket').append(rego);
	  				$('#tot_biaya').append(biaya);
	  				$('#ppn_id').append(ppn);
	  				$('#rego_paket').append(hrg_paket);

	  				if (tayang > 1) {
	  					$('#tgl_awal').show();
	  					$('#tgl_akhir').show();
	  				}

	  				$('#tgl_awal').show();

	  			},
	  			error:function () {

	  			}
			});

		});
	});
</script>

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ Helpers::img_holder() }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-media").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-media")[0] );

			$.ajax({
				url: 		$("#form-media").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-media'),'Please Wait , Memproses Order','#fff');
				}
			})
			.done(function(response) {
				$('#form-media').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : 'Order Berhasil Silahkan Melunasi Pesanan Anda'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/pasang-iklan') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-media').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

@endsection