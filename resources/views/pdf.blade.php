<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Invoice Order - Iklan Kolom - Jawa Pos</title>
        <body>
            <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>

            <table width="100%">
            
            <tr>
            <td width="50" align="center"><h1>Invoice - Iklan Kolom - Jawa Pos</h1><br><h2>Surabaya - Jawa Timur, Indonesia</h2></td>
            </tr>
            </table>
            <hr>
 
            <div style="font-family:Arial; font-size:12px;">
                <center><h2>Invoice Order</h2></center>
            </div>

            <br>
            <table class="tg">
              <tr>
                <th class="tg-3wr7">No. Order<br></th>
                <th class="tg-3wr7">Judul Iklan<br></th>
                <th class="tg-3wr7">Media Iklan<br></th>
                <th class="tg-3wr7">Kategori<br></th>
                <th class="tg-3wr7">Tanggal Tayang<br></th>
                <th class="tg-3wr7">Total Biaya</th>
              </tr>
              @foreach($data as $res => $key)
                  <tr>
                    <td class="tg-rv4w" width="10%"><center>{!! $key->order_no !!}</center></td>
                    <td class="tg-rv4w" width="10%">{!! $key->jenis->iklan_type !!}</td>
                    <td class="tg-rv4w" width="10%">{!! $key->medias->media_name !!}</td>
                    <td class="tg-rv4w" width="10%">{!! $key->categories->kategori_name !!}</td>
                    <td class="tg-rv4w" width="10%">{!! $key->tgl_muat !!}</td>
                    <td class="tg-rv4w" width="10%">Sub Total : Rp. {{ number_format($key->harga_asli) }}</td>
                  </tr>
              @endforeach
              @foreach($data as $res => $key)
                  <tr>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td>PPN : Rp. {{ number_format($key->ppn) }}</td>
                  </tr>
                  <tr>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td style="border: 0;"></td>
                    <td>Grand Total : Rp. {{ number_format($key->total_biaya) }}</td>
                  </tr>
              @endforeach

            </table>
            <br />
            
        </body>
    </head>
</html>