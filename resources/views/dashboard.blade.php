@extends('index')

@section('title')

Dashboard - Advertisement Information System

@endsection

@section('content')

<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dashboard</span> - Advertisement Information System</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
			<li class="active">Selamat Datang - {!! Auth::user()->name !!}</li>
		</ul>
	</div>
</div>

<div class="content">
	<!-- Title with subtitle -->
	<div class="panel panel-white">
		<div class="panel-heading">
			<h6 class="panel-title">Selamat Datang <small>di Advertisement Information System Panel</small></h6>
		</div>
								
		<div class="panel-body">
			Selamat Datang , {!! Auth::user()->name !!}
		</div>
	</div>

	<div class="text-center">
		Terms and Conditions
		<p />
		<pre> Pemesanan Harus Dilakukan 2 - 3 Hari Sebelum Waktu Penayangan. <p> Pembayaran Dilakukan Dengan Cara Mentransfer ke Nomor Rekening Yang Sudah Disediakan <br /> Kemudian Melakukan Konfirmasi Berupa Upload Bukti Pembayaran </pre>
	</div>

	<p />
	<!-- /title with subtitle -->

	@if(Auth::check() && Auth::user()->roles_id != 3)

	@else
	<div class="row">
		<div class="col-md-4">
			<div class="panel">
				<div class="panel-body text-center">
					<div class="icon-object border-success-400 text-success"><i class="icon-image3"></i></div>
					<h5 class="text-semibold">Jumlah Customer</h5>
					<h1> {!! $cst !!} Customer </h1>
					<a href="{{ url('/mvi-admin/customer') }}" class="btn bg-success-400">Lihat Customer</a>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="panel">
				<div class="panel-body text-center">
					<div class="icon-object border-warning-400 text-warning"><i class="icon-cart"></i></div>
					<h5 class="text-semibold">Jumlah Order</h5>
					<h1> {!! $ord !!} Order </h1>
					<a href="{{ url('/mvi-admin/order') }}" class="btn bg-warning-400">Lihat Order</a>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="panel">
				<div class="panel-body text-center">
					<div class="icon-object border-blue text-blue"><i class="icon-book"></i></div>
					<h5 class="text-semibold">Jumlah Pending Order</h5>
					<h1> {!! $pnd !!} Pending Order </h1>
					<a href="#" class="btn bg-blue">Lihat Pending Order</a>
				</div>
			</div>
		</div>
	</div>
	<!-- /main charts -->
	@endif

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection