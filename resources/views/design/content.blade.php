@extends('index')

@section('title')
{{ ($type == "create") ? 'Buat Template Baru' : 'Edit Template' }} - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/template') }}"><i class="icon-stack2 position-left"></i> Template</a></li>
			<li class="active">Data Template</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type == "create") ? 'Buat' : 'Update' }}</span> Template
		<small class="display-block">
			{{ ($type == "create") ? 'Buat Template Baru' : 'Perbarui Template' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Template</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
				@if($type == "create")
					{!! Form::open(['id' => 'form-template', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'DesignController@store', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Pilih Ukuran <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Ukuran" class="select" name="ukuran_id" id="ukuran_id">
										<option value="0" selected disabled>Pilih Ukuran</option>
										@foreach($uk as $ik => $key)
											<option value="{!! $key->ukuran_id !!}">{!! $key->ukuran_kolom !!} X {!! $key->ukuran_mm !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Pilih Jenis Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Paket" class="select" name="paket_id" id="paket_id">
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Template <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Template" name="name" 
								value="{{ ($type=='create') ? '' : $data->template_name }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Desain Template<span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="file" class="file-input-custom" name="image" accept="image/*">
									<span class="help-block">  
									NB : Ukuran Maksimum 2MB , Hanya File Gambar ( Jangan Ubah Jika Tidak Ada Perubahan )
									</span>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Template' : 'Ubah Template' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@else
					{!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['DesignController@update', base64_encode($data->template_id)], 'files' => true, 'id' => 'form-template']) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Pilih Ukuran <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Ukuran" class="select" name="ukuran_id" id="ukuran_id">
										<option value="0" selected disabled>Pilih Ukuran</option>
										@foreach($uk as $ik => $key)
											<option value="{!! $key->ukuran_id !!}">{!! $key->ukuran_kolom !!} X {!! $key->ukuran_mm !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Pilih Jenis Paket <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Jenis Paket" class="select" name="paket_id" id="paket_id">
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Template <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Template" name="name" 
								value="{{ ($type=='create') ? '' : $data->template_name }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Desain Template<span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="file" class="file-input-custom" name="image" accept="image/*">
									<span class="help-block">  
									NB : Ukuran Maksimum 2MB , Hanya File Gambar ( Jangan Ubah Jika Tidak Ada Perubahan )
									</span>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Template' : 'Ubah Template' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@endif
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script type="text/javascript">
	$(document).ready(function () {
		$('#ukuran_id').on('change', function() {

			var ukuran_id = $(this).val();
			var hasil = "";

			$.ajax({
	  			type: 'get',
	  			url : '{{ url('/mvi-admin') }}/api/a-paket?ukuran_id',
	  			data : { 'ukuran_id' : ukuran_id },
	  			success:function (data) {
	  				hasil+='<option value="0" selected disabled> Pilih Jenis Paket </option>';
	  				for (var i = 0; i < data.length; i++) {
	  					hasil+='<option value="'+data[i].paket_id+'">'+data[i].paket_name+'</option>';
	  				}

	  				$('#paket_id').append(hasil);

	  			},
	  			error:function () {
	  				sweetAlert({
						title: 	"Opss!",
						text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
						type: 	"error",
					},
					function() {
						redirect("{{ url('/') }}");
						return;
					});
	  			}
	  		});
		});
	});
</script>

<script type="text/javascript">

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ ($type=='create') ? Helpers::img_holder() : asset($data->template_design) }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-template").submit(function(e) {
			e.preventDefault();
			var formData = new FormData( $("#form-template")[0] );

			$.ajax({
				url: 		$("#form-template").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-template'),'Please Wait , {{ ($type =="create") ? "Menambahkan Template" : "Memperbarui Template" }}','#fff');
				}
			})
			.done(function(response) {
				$('#form-template').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : '{{ ($type =="create") ? "Template Berhasil Dibuat" : "Template Telah Diperbaharui" }}'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/template') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-template').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>
@endsection