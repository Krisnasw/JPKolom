@extends('index')

@section('title')

Custom Design - Advertisement Information System

@endsection

@section('style')
{!! Html::style('css/jquery.miniColors.css') !!}
{!! Html::style('css/kaos.css') !!}
@endsection

@section('content')

<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Custom Design</span> - Advertisement Information System</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Custom Design - Advertisement Information System</li>
		</ul>
	</div>
</div>

<div class="content">
	<h1 align="center">Buat Design Iklan Kamu Disini</h1>
					
	<div id="shirt">
		<div class="col-md-8">
			<div align="center" style="height: 70px;">
				<div class="clearfix">
					<div class="btn-group inline pull-left" id="texteditor">			  
						<button id="font-family" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" title="Font Style"><i class="icon-font"></i></button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="font-family-X">
							<li><a tabindex="-1" href="#" class="tfont Arial">Arial</a></li>
							<li><a tabindex="-1" href="#" class="tfont Helvetica">Helvetica</a></li>
							<li><a tabindex="-1" href="#" class="tfont MyriadPro">Myriad Pro</a></li>
							<li><a tabindex="-1" href="#" class="tfont Delicious">Delicious</a></li>
							<li><a tabindex="-1" href="#" class="tfont Verdana">Verdana</a></li>
							<li><a tabindex="-1" href="#" class="tfont Georgia">Georgia</a></li>
							<li><a tabindex="-1" href="#" class="tfont Courier">Courier</a></li>
							<li><a tabindex="-1" href="#" class="tfont ComicSansMS">Comic Sans MS</a></li>
							<li><a tabindex="-1" href="#" class="tfont Impact">Impact</a></li>
							<li><a tabindex="-1" href="#" class="tfont Monaco">Monaco</a></li>
							<li><a tabindex="-1" href="#" class="tfont Optima">Optima</a></li>
							<li><a tabindex="-1" href="#" class="tfont Hoefler Text">Hoefler Text</a></li>
							<li><a tabindex="-1" href="#" class="tfont Plaster">Plaster</a></li>
							<li><a tabindex="-1" href="#" class="tfont Engagement">Engagement</a></li>
						</ul>
						<button id="text-bold" class="btn btn-danger" data-original-title="Bold"><i class="icon-bold2"></i></button>
						<button id="text-italic" class="btn btn-danger" data-original-title="Italic"><i class="icon-italic2"></i></button>
						<button id="text-strike" class="btn btn-danger" title="Strike" style=""><i class=" icon-strikethrough3"></i></button>
						<button id="text-underline" class="btn btn-danger" title="Underline" style=""><i class="icon-underline2"></i></button>
						<a class="btn btn-danger" href="#" rel="tooltip" data-placement="top" data-original-title="Font Color"><input type="hidden" id="text-fontcolor" class="color-picker" size="7" value="#000000"></a>
						<a class="btn btn-danger" href="#" rel="tooltip" data-placement="top" data-original-title="Font Border Color"><input type="hidden" id="text-strokecolor" class="color-picker" size="7" value="#000000"></a>
						<a class="btn btn-danger" href="#" rel="tooltip" data-placement="top" data-original-title="Font Background Color"><input type="hidden" id="text-bgcolor" class="color-picker" size="7" value="#ffffff"></a>
					</div>
					<div class="pull-right" align="" id="imageeditor">
						<div class="btn-group">
							<button class="btn btn-danger" id="bring-to-front" title="Bring to Front"><i class="icon-backward rotate" style="height:19px;"></i></button>
							<button class="btn btn-danger" id="send-to-back" title="Send to Back"><i class="icon-forward rotate" style="height:19px;"></i></button>
							<button id="remove-selected" class="btn btn-danger" title="Delete selected item"><i class="icon-trash" style="height:19px;"></i></button>
						</div>
					</div>
				</div>
			</div>
		
			<center>
				<div id="drawingArea" style="width: 530px; height: 630px; position: relative; background-color: #dedede;">
					<canvas id="tcanvas" width="530" height="630" style="-webkit-user-select: none;"></canvas>
				</div>
			</center>
		</div>
						
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					Template
				</div>
				<div class="panel-body">
					<div class="input-group">
						<select id="ukuran_id" class="form-control">
							<option value="0" selected disabled>Pilih Ukuran Iklan</option>
							@foreach($uk as $res => $key)
								<option value="{!! $key->ukuran_id !!}">{!! $key->ukuran_kolom !!} X {!! $key->ukuran_mm !!}</option>
							@endforeach
						</select>
						<span class="input-group-btn">
							<button id="flip" type="submit" class="btn btn-primary" title="Show Back View"><i class="icon-eye"></i></button>
						</span>
					</div>

					<br />

					<div class="input-group">
						<select id="paket_id" class="form-control">
						</select>
						<span class="input-group-btn">
							<button id="flip" type="submit" class="btn btn-primary" title="Show Back View"><i class="icon-eye"></i></button>
						</span>
					</div>

					<br />

					<div class="input-group">
						<select id="tshirtTypes" class="form-control">
						</select>
						<span class="input-group-btn">
							<button id="flip" type="submit" class="btn btn-primary" title="Show Back View"><i class="icon-eye"></i></button>
						</span>
					</div>
				</div>
			</div>
{{-- 				<div class="panel panel-default">
					<div class="panel-heading">
					Pilih Warna
					</div>
					<div class="panel-body">
						<input type="text" id="ts-bcolor" class="color-picker" size="7" value="#ffffff">
					</div>
				</div> --}}

				<div class="panel panel-default">
					<div class="panel-heading">
						Text
					</div>
					<div class="panel-body">
						<div class="input-group">
							<input type="text" class="form-control" id="text-string" placeholder="Add Text Here ..">
							<span class="input-group-btn"><button type="submit" id="add-text" class="btn btn-primary">Add</button></span>
						</div>
						<p />
						<div class="input-group">
							<span class="input-group-addon">Effect</span>
							<select class="form-control" name="effect" id="effect" >
								<option value="STRAIGHT">Straight</option>
								<option value="curved">Curved</option>
								<option value="arc">Arc</option>
								<option value="smallToLarge">Small To Large</option>
								<option value="largeToSmallTop">Large To Small Top</option>
								<option value="largeToSmallBottom">Large To Small Bottom</option>
								<option value="bulge">Bulge</option>
							</select>
						</div>
						<div id="curverdText"><br>
							Reverse : <input type="checkbox" name="reverse" id="reverse" /><br><br>
							Radius : <input type="range"  min="0" max="100" value="50" id="radius" /><br>
							Spacing : <input type="range"  min="5" max="40" value="20" id="spacing" /><br>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Image
					</div>
					<div class="panel-body">
						<div class="form-group">
							<input type="file" id="inputImg" class="file-styled">
						</div>
					</div>
				</div>
			<input type="submit" id="downloadImg" class="btn btn-info pull-right" value="Download"/>
		</div>
	</div>
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('js/fabric.js') !!}
{!! Html::script('js/fabric.curvedText.js') !!}
{!! Html::script('js/jquery.miniColors.min.js') !!}
<script type="text/javascript">
    jQuery.browser = {};
    (function () {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();
</script>
<script type="text/javascript">
	$(document).ready(function () {

	var canvas,
        a,
        b;

    canvas = new fabric.Canvas('tcanvas', {
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor: 'blue'
    });

    function getRandomNum(min, max) {
        return Math.random() * (max - min) + min;
    }

    function onObjectSelected(e) {
        var selectedObject = e.target;
        $("#text-string").val("");
        selectedObject.hasRotatingPoint = true
        if (selectedObject && selectedObject.type === 'curvedText') {
            //display text editor	    	
            $("#texteditor").css('display', 'block');
            $("#text-string").val(selectedObject.getText());
            $('#text-bgcolor').miniColors('value', selectedObject.backgroundColor);
            $('#text-fontcolor').miniColors('value', selectedObject.fill);
            $('#text-strokecolor').miniColors('value', selectedObject.stroke);
            $("#imageeditor").css('display', 'block');
			$('#effect').val(selectedObject.getEffect());	
			if(selectedObject.getEffect() == 'curved' || selectedObject.getEffect() == 'arc'){
				$('#reverse').prop('checked', selectedObject.get('reverse'));
				$('#radius').val(selectedObject.get('radius'));
				$('#spacing').val(selectedObject.get('spacing'));
				$("#curverdText").css('display', 'block');
			}else{			
				$("#curverdText").css('display', 'none');
			}
        } else if (selectedObject && selectedObject.type === 'image') {
            //display image editor
            $("#texteditor").css('display', 'none');
            $("#imageeditor").css('display', 'block');
        }
    }

    function onSelectedCleared(e) {
        $("#texteditor").css('display', 'none');
        $("#text-string").val("");
        $("#imageeditor").css('display', 'none');
    }

    function removeWhite() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && activeObject.type === 'image') {
            activeObject.filters[2] = new fabric.Image.filters.RemoveWhite({
                hreshold: 100,
                distance: 10
            }); //0-255, 0-255
            activeObject.applyFilters(canvas.renderAll.bind(canvas));
        }
    }
	
	function convertDataURIToBinary(dataURI) {
		var BASE64_MARKER = ';base64,';
		var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
		var base64 = dataURI.substring(base64Index);
		var raw = window.atob(base64);
		var rawLength = raw.length;
		var array = new Uint8Array(new ArrayBuffer(rawLength));

		for(i = 0; i < rawLength; i++) {
			array[i] = raw.charCodeAt(i);
		}
		return array;
	}

	var downloadFile = (function () {
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		return function () {
			var data = convertDataURIToBinary(canvas.toDataURL('png')), 
				fileName = 'image.png',
				blob = new Blob([data], {type: 'image/png'}),
				url = window.URL.createObjectURL(blob);
			a.href = url;
			a.download = fileName;
			a.click();
			window.URL.revokeObjectURL(url);
		};
	}());
	document.getElementById('downloadImg').addEventListener('click', downloadFile, false);

    function readFile(evt) {
        var f = evt.target.files[0];
        if (f) {
            if (/(jpe?g|png|gif)$/i.test(f.type)) {
                var r = new FileReader();
                r.onload = function(e) {
                    var base64Img = e.target.result;
                    var offset = 50;
                    var left = fabric.util.getRandomInt(0 + offset, 200 - offset);
                    var top = fabric.util.getRandomInt(0 + offset, 400 - offset);
                    var angle = fabric.util.getRandomInt(-20, 40);
                    var width = fabric.util.getRandomInt(30, 50);
                    var opacity = (function(min, max) {
                        return Math.random() * (max - min) + min;
                    })(0.5, 1);
                    fabric.Image.fromURL(base64Img, function(image) {
                        image.set({
                            left: left,
                            top: top,
                            angle: 0,
                            padding: 10,
                            cornersize: 10,
                            hasRotatingPoint: true
                        });
                        canvas.add(image);
                    });
                }
                r.readAsDataURL(f);
            } else {
                alert("Failed file type");
            }
        } else {
            alert("Failed to load file");
        }
    }

		$('#ukuran_id').on('change', function() {

			var ukuran_id = $(this).val();
			var hasil = "";

			$.ajax({
	  			type: 'get',
	  			url : '{{ url('/mvi-admin') }}/api/a-paket?ukuran_id',
	  			data : { 'ukuran_id' : ukuran_id },
	  			success:function (data) {
	  				hasil+='<option value="0" selected disabled> Pilih Jenis Paket </option>';
	  				for (var i = 0; i < data.length; i++) {
	  					hasil+='<option value="'+data[i].paket_id+'">'+data[i].paket_name+'</option>';
	  				}

	  				$('#paket_id').append(hasil);

	  			},
	  			error:function () {
	  				sweetAlert({
						title: 	"Opss!",
						text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
						type: 	"error",
					},
					function() {
						redirect("{{ url('/') }}");
						return;
					});
	  			}
	  		});
		});

		$('#paket_id').on('change', function() {

			var paket_id = $(this).val();
			var hasil = "";
			var design = "";

			$.ajax({
				type: 'get',
				url: '{{ url('/mvi-admin') }}/api/a-template?paket_id',
				data: { 'paket_id' : paket_id },
				success: function (data) {
	  				hasil+='<option value="0" selected disabled> Pilih Jenis Template </option>';
	  				for (var i = 0; i < data.length; i++) {
	  					hasil+='<option value="'+data[i].template_id+'">'+data[i].template_name+'</option>';
	  					design+=data[i].template_design;
	  				}

	  				$('#tshirtTypes').append(hasil);
	  				canvas.on({
				        'object:moving': function(e) {
				            e.target.opacity = 0.5
				        },
				        'object:modified': function(e) {
				            e.target.opacity = 1
				        },
				        'object:selected': function(e) {
				        	e.target
				        },
				        'selection:cleared':function(e) {

				        },
				    });
				    canvas.findTarget = (function(b) {
				        return function() {
				            var a = b.apply(this, arguments);
				            if (a) {
				                if (this._hoveredTarget !== a) {
				                    canvas.fire('object:over', {
				                        target: a
				                    });
				                    if (this._hoveredTarget) {
				                        canvas.fire('object:out', {
				                            target: this._hoveredTarget
				                        })
				                    }
				                    this._hoveredTarget = a
				                }
				            } else if (this._hoveredTarget) {
				                canvas.fire('object:out', {
				                    target: this._hoveredTarget
				                });
				                this._hoveredTarget = null
				            }
				            return a
				        }
				    })(canvas.findTarget);
				    canvas.setBackgroundImage('../../' + design + '', canvas.renderAll.bind(canvas), {
				        backgroundImageStretch: false
				    });
				    canvas.on('object:over', function(e) {});
    				canvas.on('object:out', function(e) {});
    				document.getElementById('add-text').onclick = function() {
				        var a = $("#text-string").val();
				        var b = new fabric.CurvedText(a, {
				            left: fabric.util.getRandomInt(0, 200),
				            top: fabric.util.getRandomInt(0, 400),
				            fontFamily: 'helvetica',
				            angle: 0,
				            fill: '#000000',
				            scaleX: 0.5,
				            scaleY: 0.5,
							effect: $("#effect").val(),
				            fontWeight: '',
				            hasRotatingPoint: true
				        });
				        canvas.add(b);
				        canvas.item(canvas.item.length - 1).hasRotatingPoint = true;
						if($("#effect").val() == 'curved' || $("#effect").val() == 'arc'){	
							$("#curverdText").css('display', 'block');
						}else{			
							$("#curverdText").css('display', 'none');
						}
				        $("#texteditor").css('display', 'block');
				        $("#imageeditor").css('display', 'block')
				    };
					$('#radius, #spacing, #effect').change(function(){
						var obj = canvas.getActiveObject();
						if(obj){
							obj.set($(this).attr('id'),$(this).val()); 
							if($('#effect').val() == 'curved' || $('#effect').val() == 'arc'){	
								$("#curverdText").css('display', 'block');
							}else{			
								$("#curverdText").css('display', 'none');
							}
						}
						canvas.renderAll();
					});
					$('#reverse').click(function(){
						var obj = canvas.getActiveObject();
						if(obj){
							obj.set('reverse',$(this).is(':checked')); 
							canvas.renderAll();
						}
					});
				    $("#text-string").keyup(function() {
				        var a = canvas.getActiveObject();
				        if (a && a.type === 'curvedText') {
							a.setText(this.value); 
				            canvas.renderAll()
				        }
				    });
				    $(".tfont").click(function() {
				        var a = canvas.getActiveObject();
				        if (a && a.type === 'curvedText') {
							a.set('fontFamily', $(this).html()); 
				            canvas.renderAll();
				        }
						return false;
				    });
				    document.getElementById('remove-selected').onclick = function() {
				        var b = canvas.getActiveObject(),
				            activeGroup = canvas.getActiveGroup();
				        if (b) {
				            canvas.remove(b);
				            $("#text-string").val("")
				        } else if (activeGroup) {
				            var c = activeGroup.getObjects();
				            canvas.discardActiveGroup();
				            c.forEach(function(a) {
				                canvas.remove(a)
				            })
				        }
				    };
				    document.getElementById('bring-to-front').onclick = function() {
				        var b = canvas.getActiveObject(),
				            activeGroup = canvas.getActiveGroup();
				        if (b) {
				            canvas.bringToFront(b)
				        } else if (activeGroup) {
				            var c = activeGroup.getObjects();
				            canvas.discardActiveGroup();
				            c.forEach(function(a) {
				                canvas.bringToFront(a)
				            })
				        }
				    };
				    document.getElementById('send-to-back').onclick = function() {
				        var b = canvas.getActiveObject(),
				            activeGroup = canvas.getActiveGroup();
				        if (b) {
				             canvas.sendToBack(b)
				        } else if (activeGroup) {
				            var c = activeGroup.getObjects();
				            canvas.discardActiveGroup();
				            c.forEach(function(a) {
				                canvas.sendToBack(a)
				            })
				        }
				    };
				    $("#text-bold").click(function() {
				        var a = canvas.getActiveObject();
				        if (a && a.type === 'curvedText') {
							a.set('fontWeight', (a.fontWeight == 'bold' ? '' : 'bold')); 
				            canvas.renderAll()
				        }
				    });
				    $("#text-italic").click(function() {
				        var a = canvas.getActiveObject();
				        if (a && a.type === 'curvedText') {
							a.set('fontStyle', (a.fontStyle == 'italic' ? '' : 'italic')); 
				            canvas.renderAll()
				        }
				    });
				    $("#text-strike").click(function() {
				        var a = canvas.getActiveObject();
				        if (a && a.type === 'curvedText') {
							a.set('textDecoration', (a.textDecoration == 'line-through' ? '' : 'line-through')); 
				            canvas.renderAll()
				        }
				    });
				    $("#text-underline").click(function() {
				        var a = canvas.getActiveObject();
				        if (a && a.type === 'curvedText') {
							a.set('textDecoration', (a.textDecoration == 'underline' ? '' : 'underline')); 
				            canvas.renderAll()
				        }
				    });
				    $('#ts-bcolor').miniColors({
				        change: function(a, b) {
				            canvas.backgroundColor = this.value;
				            canvas.renderAll();
				        },
				        open: function(a, b) {},
				        close: function(a, b) {}
				    });
				    $('#text-bgcolor').miniColors({
				        change: function(a, b) {
				            var c = canvas.getActiveObject();
				            if (c && c.type === 'curvedText') {
								c.set('backgroundColor', this.value);
				                canvas.renderAll()
				            }
				        },
				        open: function(a, b) {},
				        close: function(a, b) {}
				    });
				    $('#text-fontcolor').miniColors({
				        change: function(a, b) {
				            var c = canvas.getActiveObject();
				            if (c && c.type === 'curvedText') {
								c.set('fill', this.value);
				                canvas.renderAll()
				            }
				        },
				        open: function(a, b) {},
				        close: function(a, b) {}
				    });
				    $('#text-strokecolor').miniColors({
				        change: function(a, b) {
				            var c = canvas.getActiveObject();
				            if (c && c.type === 'curvedText') {
								c.set('stroke', this.value);
				                canvas.renderAll()
				            }
				        },
				        open: function(a, b) {},
				        close: function(a, b) {}
				    });

				    document.getElementById('inputImg').addEventListener('change', readFile, false);
				    $("#tshirtTypes").change(function (e) {
				        canvas.setBackgroundImage('../../' + design + '', canvas.renderAll.bind(canvas), {
				            backgroundImageStretch: false
				        });
				    });
				},
				error: function () {
					sweetAlert({
						title: "Opss!",
						text: "Ada Yang Salah!, Silahkan Coba Lagi",
						type: "error",
					},
					function() {
						redirect("{{ url('/mvi-admin/new-design') }}");
						return;
					});
				}
			});

		});
	});
</script>
@endsection