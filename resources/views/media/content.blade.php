@extends('index')

@section('title')
{{ ($type == "create") ? 'Buat Media Baru' : 'Edit Media' }} - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/media') }}"><i class="icon-stack2 position-left"></i> Media</a></li>
			<li class="active">Data Media</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type == "create") ? 'Buat' : 'Update' }}</span> Media
		<small class="display-block">
			{{ ($type == "create") ? 'Buat Media Baru' : 'Perbarui Media' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Media</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
				@if($type == "create")
					{!! Form::open(['id' => 'form-media', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'MediaController@store', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Media <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Media" name="media_name" 
								value="{{ ($type=='create') ? '' : $data->media_name }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Media' : 'Ubah Media' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@else
					{!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['MediaController@update', base64_encode($data->media_id)], 'files' => true, 'id' => 'form-media']) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Media <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Media" name="media_name" 
								value="{{ ($type=='create') ? '' : $data->media_name }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Media' : 'Ubah Media' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@endif
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script>
  (function($){
    $(function() {
      $("#email").emailautocomplete({
        domains: ["example.com"] //add your own domains
      });
    });
  }(jQuery));
</script>

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ ($type=='create') ? Helpers::img_holder() : asset($data->image) }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-media").submit(function(e) {
			e.preventDefault();
			var formData = new FormData( $("#form-media")[0] );

			$.ajax({
				url: 		$("#form-media").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-media'),'Please Wait , {{ ($type =="create") ? "Menambahkan Media" : "Memperbarui Media" }}','#fff');
				}
			})
			.done(function(response) {
				$('#form-media').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : '{{ ($type =="create") ? "Media Berhasil Dibuat" : "Media Telah Diperbaharui" }}'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/media') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-media').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

{!! Html::script('admin_assets/js/pages/form_select2.js') !!}
@endsection