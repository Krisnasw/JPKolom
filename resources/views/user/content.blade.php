@extends('index')

@section('title')
{{ ($type == "create") ? 'Buat Customer Baru' : 'Edit Customer' }} - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/customer') }}"><i class="icon-stack2 position-left"></i> Customer</a></li>
			<li class="active">Data Customer</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type == "create") ? 'Buat' : 'Update' }}</span> Customer
		<small class="display-block">
			{{ ($type == "create") ? 'Buat Customer Baru' : 'Perbarui Customer' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Customer</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
				@if($type == "create")
					{!! Form::open(['id' => 'form-user', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'UserController@store', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Lengkap <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Lengkap" name="name" 
								value="{{ ($type=='create') ? '' : $data->name }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Alamat Lengkap <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Alamat Lengkap" name="address" 
								value="{{ ($type=='create') ? '' : $data->address }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">No. Handphone <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan No. Handphone" name="contact" 
								value="{{ ($type=='create') ? '' : $data->contact }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Username <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Username" name="username" 
								value="{{ ($type=='create') ? '' : $data->username }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Email <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" id="email" type="email" placeholder="Masukkan Email" name="email" 
								value="{{ ($type=='create') ? '' : $data->email }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Password <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="password" placeholder="Masukkan Password" name="password" 
								value="{{ ($type=='create') ? '' : $data->password }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Foto Profil<span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="file" class="file-input-custom" name="image" accept="image/*" {{ ($type=='create') ? '' : '' }}>
									<span class="help-block"> {{ ($type=='create') ? '' : '( Jangan Ubah Jika Tidak Ada Perubahan)' }}  
									Ukuran Maksimum 2MB , Hanya File Gambar
									</span>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Customer' : 'Ubah Customer' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@else
					{!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['UserController@update', base64_encode($data->id)], 'files' => true, 'id' => 'form-user']) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Lengkap <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Lengkap" name="name" 
								value="{{ ($type=='create') ? '' : $data->name }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Alamat Lengkap <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Alamat Lengkap" name="address" 
								value="{{ ($type=='create') ? '' : $data->address }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">No. Handphone <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan No. Handphone" name="contact" 
								value="{{ ($type=='create') ? '' : $data->contact }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Username <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Username" name="username" 
								value="{{ ($type=='create') ? '' : $data->username }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Email <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" id="email" type="email" placeholder="Masukkan Email" name="email" 
								value="{{ ($type=='create') ? '' : $data->email }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Password <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="password" placeholder="Masukkan Password" name="password" 
								value="{{ ($type=='create') ? '' : $data->password }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Foto Profil<span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input type="file" class="file-input-custom" name="image" accept="image/*" {{ ($type=='create') ? '' : '' }}>
									<span class="help-block"> {{ ($type=='create') ? '' : '( Jangan Ubah Jika Tidak Ada Perubahan)' }}  
									Ukuran Maksimum 2MB , Hanya File Gambar
									</span>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Customer' : 'Ubah Customer' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@endif
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script>
  (function($){
    $(function() {
      $("#email").emailautocomplete({
        domains: ["example.com"] //add your own domains
      });
    });
  }(jQuery));
</script>

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ ($type=='create') ? Helpers::img_holder() : asset($data->image) }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-user").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-user")[0] );

			$.ajax({
				url: 		$("#form-user").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-user'),'Please Wait , {{ ($type =="create") ? "Menambahkan Customer" : "Memperbarui Customer" }}','#fff');
				}
			})
			.done(function(response) {
				$('#form-user').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : '{{ ($type =="create") ? "Customer Berhasil Dibuat" : "Customer Telah Diperbaharui" }}'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/customer') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-user').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

{!! Html::script('admin_assets/js/pages/form_select2.js') !!}
@endsection