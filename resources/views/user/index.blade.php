@extends('index')

@section('title')
Advertisement Information System - Daftar Customer
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/customer') }}"><i class="icon-stack2 position-left"></i> Customer</a></li>
			<li class="active">Data Customer</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user-tie"></i> Daftar</span> Customer
		<small class="display-block">List Daftar Customer</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Daftar Customer Anda</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>No</th>
                            <th>Foto Profil</th>
                            <th>Nama Lengkap</th>
                            <th>Alamat Lengkap</th>
                            <th>No. Handphone</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@php $no = 0; @endphp
                    	@foreach($data as $key => $result)
                         <tr>
                        	<td align="center">{!! ++$no !!}</td>
	                        <td style="width:300px;">
	                        	<a href="{{ asset($result->image) }}" data-popup="lightbox">
					                <img src="{{ asset($result->image) }}" alt="" class="img-rounded img-preview">
				                </a>
	                        </td>
	                        <td align="left">{!! Helpers::read_more($result->name) !!}</td>
	                        <td align="left">{!! Helpers::read_more($result->address) !!}</td>
	                        <td align="left">{!! Helpers::read_more($result->contact) !!}</td>
	                        <td align="left">{!! $result->email !!}</td>
	                        <td align="left">{!! ($result->status == 'y') ? '<label class="label label-primary"> Aktif </label>' : '<label class="label label-danger"> Belum Aktif </label>' !!}
	                        <td class="text-center">
	                           <div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a href="{{ url('/mvi-admin/customer') }}/{!! base64_encode($result->id) !!}/edit">
												<i class="fa fa-edit"></i> Ubah Customer
											</a>
										</li>
										<li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_theme_danger{{{ base64_decode($result->id) }}}">
												<i class="fa fa-trash"></i> Hapus Customer
											</a>
										</li>
									</ul>
								</div>
	                        </td>
                        </tr>

                        <!-- Danger modal -->
						<div id="modal_theme_danger{{{ base64_decode($result->id) }}}" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-danger">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h6 class="modal-title">Hapus Data</h6>
									</div>

									<div class="modal-body">
										<h6 class="text-semibold">Apakah Anda Yaking Menghapus <i> {!! $result->name !!} </i></h6>
										<p>NB : Data Tidak Dapat Kembali Setelah Anda Hapus !</p>
									</div>

									<div class="modal-footer">
										{!! Form::open(array('method' => 'DELETE', 'route' => array('customer.destroy', base64_encode($result->id)))) !!}
			                            	{!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
			                            {!! Form::close() !!}
										<button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
{!! Html::script('admin_assets/js/plugins/media/fancybox.min.js') !!}
{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}
{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}
{!! Html::script('admin_assets/js/plugins/tables/datatables/datatables.min.js') !!}
{!! Html::script('admin_assets/js/pages/gallery_library.js') !!}
@endsection