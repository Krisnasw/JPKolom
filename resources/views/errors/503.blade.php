<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Title -->
    <title>503 - Be Right Back.</title>
    <!-- CSS Links -->
    <link rel="stylesheet" href="{{ asset('css/main-dark.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- Fonts -->
    <style>
        @import 'https://fonts.googleapis.com/css?family=Share+Tech+Mono|Space+Mono';
    </style>
</head>
<body>
    <div class="container">
        <!-- Overlay -->
        <div class="overlay" id="particles-js"></div>
        <!-- Content -->
        <div class="content">
            <div class="message">
                <p class="message-heading">Be Right Back.</p>
                <p class="message-description">Harap Sabar, Terjadi Kesalahan Teknis</p>
            </div>
            <div class="links">
                <a href="{{ url('/') }}"></a>
                <a href="#"></a>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/particles.js') }}"></script>
    <script>
        "use strict"
        particlesJS.load('particles-js', '{{ asset('js/particles-dark.json') }}', function() {
            console.log('callback - particles.js config loaded');
        });
    </script>
</body>
</html>