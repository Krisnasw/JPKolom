@extends('index')

@section('title')
{{ ($type == "create") ? 'Buat Ukuran Baru' : 'Edit Ukuran' }} - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/ukuran') }}"><i class="icon-stack2 position-left"></i> Ukuran</a></li>
			<li class="active">Data Ukuran</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type == "create") ? 'Buat' : 'Update' }}</span> Ukuran
		<small class="display-block">
			{{ ($type == "create") ? 'Buat Ukuran Baru' : 'Perbarui Ukuran' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Ukuran</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
				@if($type == "create")
					{!! Form::open(['id' => 'form-ukuran', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'UkuranController@store', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Ukuran Kolom <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Ukuran Kolom" name="ukuran_kolom" 
								value="{{ ($type=='create') ? '' : $data->ukuran_kolom }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Ukuran MM <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Ukuran MM" name="ukuran_mm" 
								value="{{ ($type=='create') ? '' : $data->ukuran_mm }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Ukuran' : 'Ubah Ukuran' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@else
					{!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['UkuranController@update', base64_encode($data->ukuran_id)], 'files' => true, 'id' => 'form-ukuran']) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Ukuran Kolom <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Ukuran Kolom" name="ukuran_kolom" 
								value="{{ ($type=='create') ? '' : $data->ukuran_kolom }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Ukuran MM <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Ukuran MM" name="ukuran_mm" 
								value="{{ ($type=='create') ? '' : $data->ukuran_mm }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Ukuran' : 'Ubah Ukuran' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@endif
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script>
  (function($){
    $(function() {
      $("#email").emailautocomplete({
        domains: ["example.com"] //add your own domains
      });
    });
  }(jQuery));
</script>

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ ($type=='create') ? Helpers::img_holder() : asset($data->image) }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-ukuran").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-ukuran")[0] );

			$.ajax({
				url: 		$("#form-ukuran").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-ukuran'),'Please Wait , {{ ($type =="create") ? "Menambahkan Ukuran" : "Memperbarui Ukuran" }}','#fff');
				}
			})
			.done(function(response) {
				$('#form-ukuran').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : '{{ ($type =="create") ? "Ukuran Berhasil Dibuat" : "Ukuran Telah Diperbaharui" }}'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/ukuran') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-ukuran').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

{!! Html::script('admin_assets/js/pages/form_select2.js') !!}
@endsection