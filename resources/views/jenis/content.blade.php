@extends('index')

@section('title')
{{ ($type == "create") ? 'Buat Jenis Iklan Baru' : 'Edit Jenis Iklan' }} - Advertisement Information System
@endsection

@section('content')
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{ url('/mvi-admin/jenis-iklan') }}"><i class="icon-stack2 position-left"></i> Jenis Iklan</a></li>
			<li class="active">Data Jenis Iklan</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type == "create") ? 'Buat' : 'Update' }}</span> Jenis Iklan
		<small class="display-block">
			{{ ($type == "create") ? 'Buat Jenis Iklan Baru' : 'Perbarui Jenis Iklan' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Jenis Iklan</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
				@if($type == "create")
					{!! Form::open(['id' => 'form-jenis', 'class' => 'form-horizontal', 'method' => 'POST', 'action' => 'IklanController@store', 'files' => true]) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Media Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Media Iklan" class="select" name="jenis">
										<option value="selected">Pilih Media Iklan</option>
										@foreach($media as $med => $key)
											<option value="{!! $key->media_id !!}">{!! $key->media_name !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Tipe Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Tipe Iklan" name="iklan_type" 
								value="{{ ($type=='create') ? '' : $data->iklan_type }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Jenis Iklan' : 'Ubah Jenis Iklan' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@else
					{!! Form::model($data, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['IklanController@update', base64_encode($data->iklan_id)], 'files' => true, 'id' => 'form-jenis']) !!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Media Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<select data-placeholder="Pilih Media Iklan" class="select" name="jenis">
										<option value="selected">Pilih Media Iklan</option>
										@foreach($media as $med => $key)
											<option value="{!! $key->media_id !!}">{!! $key->media_name !!}</option>
										@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Nama Tipe Iklan <span class="text-danger"><b>*</b></span></label>
							<div class="col-lg-10">
								<input class="form-control" type="text" placeholder="Masukkan Nama Tipe Iklan" name="iklan_type" 
								value="{{ ($type=='create') ? '' : $data->iklan_type }}" required>
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Jenis Iklan' : 'Ubah Jenis Iklan' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
								<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					{!! Form::close() !!}
				@endif
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

{!! Html::script('admin_assets/js/plugins/uploaders/fileinput.min.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/selects/select2.min.js') !!}
{!! Html::script('admin_assets/js/pages/form_layouts.js') !!}

<script type="text/javascript">
	var editorsmall = false;
</script>

{!! Html::script('admin_assets/js/pages/uploader_bootstrap.js') !!}
{!! Html::script('admin_assets/js/plugins/forms/styling/switch.min.js') !!}

<script>
  (function($){
    $(function() {
      $("#email").emailautocomplete({
        domains: ["example.com"] //add your own domains
      });
    });
  }(jQuery));
</script>

<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	$(document).ready(function(){
			$('.file-input-custom').fileinput({
	        previewFileType: 'image',
	        browseLabel: 'Select',
	        browseClass: 'btn bg-slate-700',
	        browseIcon: '<i class="icon-image2 position-left"></i> ',
	        removeLabel: 'Remove',
	        removeClass: 'btn btn-danger',
	        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
	        uploadClass: 'hidden',
	        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
	        layoutTemplates: {
	            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
	        },
	        initialPreview: ["<img src='{{ ($type=='create') ? Helpers::img_holder() : asset($data->image) }}' class='file-preview-image' alt=''>",],
	        overwriteInitial: true
	    });
	})

	$("#form-jenis").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-jenis")[0] );

			$.ajax({
				url: 		$("#form-jenis").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function() {
					blockMessage($('#form-jenis'),'Please Wait , {{ ($type =="create") ? "Menambahkan Jenis Iklan" : "Memperbarui Jenis Iklan" }}','#fff');
				}
			})
			.done(function(response) {
				$('#form-jenis').unblock();
				sweetAlert({
					title: 	((response.status == false) ? "Opps!" : '{{ ($type =="create") ? "Jenis Iklan Berhasil Dibuat" : "Jenis Iklan Telah Diperbaharui" }}'),
					text: 	response.msg,
					type: 	((response.status == false) ? "error" : "success"),
				},
				function(){
					if(response.status != false) {
						redirect("{{ url('/mvi-admin/jenis-iklan') }}");
						return;
					}
				});
			})
			.fail(function() {
			    $('#form-jenis').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){

				});
			 })
		})
</script>

{!! Html::script('admin_assets/js/pages/form_select2.js') !!}
@endsection